Mode and dynamics of vanA-type vancomycin-resistance dissemination in
Dutch hospitals
================
Sergio Arredondo-Alonso
20/11/2020

# R libraries

``` r
library(tidyverse)
library(ggplot2) # For plotting purposes
library(ggridges) # For plotting the distribution of the data 
library(cowplot) # To arrange the plots side by side
library(ggtree) # To plot the phylogenetic trees together with metadata
library(ape) # To read the newick trees 
library(igraph) # To do the network representation
library(rcartocolor) # To have enough colours in the plots
library(maps) # To plot the spatial information 
library(scatterpie) # To draw the pie charts 
library(RColorBrewer) # To generate a second palette of colours to draw the Tn1546 variants 
library(gplots) # To draw heatmaps 
```

# Metadata

``` r
# Metadata information present in the E. faecium collection
isolates_metadata <- read.csv(file = '../data/metadata.csv')
colnames(isolates_metadata)[2] <- 'Strain'

# Subsetting only Dutch isolates
nld_isolates <- subset(isolates_metadata, isolates_metadata$country.of.isolation == 'NLD')

# Subsetting Dutch isolates between 2012-2015
nld_period <- subset(isolates_metadata, isolates_metadata$year %in% c("2012","2013","2014","2015"))

# Considering only hospitalized patients
nld_hosp_period <- subset(nld_period, nld_period$isolation.source == "Hospitalized patient")


# Vector of colours, only for plotting purposes

col_vector <- NULL

# Defining a color palette which is color blind friendly

nColor <- 12 # Maximum number of colours available 
pal <- carto_pal(nColor, "Safe") #Using the safe palette 

# These are the most predominant SCs in the dataset, the SCs 32, 7, 19, 23, 5 and 9 were given other complementary colours since the palette only provides a maximum of 12 colours

col_vector[13] <- pal[1]
col_vector[17] <- pal[2]
col_vector[18] <- pal[3]
col_vector[10] <- pal[4]
col_vector[1] <- pal[5]
col_vector[20] <- pal[6]
col_vector[2] <- pal[7]
col_vector[22] <- pal[8]
col_vector[29] <- pal[9]
col_vector[8] <- pal[10]
col_vector[6] <- pal[11]
col_vector[30] <- pal[12]
col_vector[32] <- 'gray90'
col_vector[7] <- 'orange'
col_vector[19] <- 'black'
col_vector[23] <- 'red'
col_vector[5] <- 'green'
col_vector[9] <- 'salmon'
```

# Analysis

## Distribution of SC along time

``` r
# Selecting a few columns of all the metadata information

nld_pop_baps <- subset(nld_hosp_period, select = c('Strain', 'Isolation.date','MLST','postBNGBAPS.2','postBNGBAPS.3','Insilico_van_gene','Longitude','Latitude','Hospital'))


# Parsing the isolation dates
dates <- as.character(nld_pop_baps$Isolation.date)

nld_pop_baps$Isolation.date <- as.Date(dates, "%d/%m/%Y")

nld_pop_baps$Year <- str_split_fixed(string = nld_pop_baps$Isolation.date, pattern = '-', n = 3)[,1]

nld_pop_baps <- subset(nld_pop_baps, nld_pop_baps$Year %in% c("2012","2013","2014","2015"))

# Selecting the isolates containing the vanA resistance gene cluster

vana_isolates <- subset(nld_pop_baps, nld_pop_baps$Insilico_van_gene == 'vanA')

# Saving the metadata information 

table_S1 <- vana_isolates
table_S1$MLST <- NULL
table_S1$postBNGBAPS.3 <- NULL
table_S1$Year <- NULL
table_S1$Insilico_van_gene <- NULL

colnames(table_S1) <- c('Isolate','Isolation_date','hierBAPS_SC','Longitude','Latitude','Hospital')

# We saved this metadata information and provided to the readers as Additional File S1 

#write.csv(x = table_S1, file = '/home/sergi/Documents/Papers_and_Abstracts/Dissemination_vanA/Genome_Medicine/Rebuttal/Additional_File_S1.csv', quote = FALSE, row.names = FALSE)


## Dutch isolates
nrow(nld_isolates)
```

    ## [1] 1066

``` r
## Dutch isolates from 2012-2015
nrow(nld_period)
```

    ## [1] 855

``` r
## Dutch isolates from 2012-2015 which correspond to hospitalized patients
nrow(nld_hosp_period)
```

    ## [1] 718

``` r
## Isolates from which we have complete metadata information 

nrow(nld_pop_baps)
```

    ## [1] 593

``` r
## vancomycin resistance found in these isolates

nld_pop_baps %>%
  group_by(Insilico_van_gene) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Insilico_van_gene"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"","2":"14"},{"1":"vanA","2":"309"},{"1":"vanA/vanB","2":"5"},{"1":"vanB","2":"265"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Distribution of SC counts

plot_sc_counts <- ggplot(vana_isolates, aes(x=as.factor(postBNGBAPS.2), fill = as.factor(postBNGBAPS.2))) + 
  geom_bar(stat="count", width=0.7) +
  scale_fill_manual(values=col_vector[sort(unique(vana_isolates$postBNGBAPS.2))]) +
  theme_bw() + 
  facet_wrap(. ~ Year, nrow = 1, ncol = 4) +
  labs(x = 'Sequencing cluster (SC)', y = 'Count', fill = 'SC') +
  theme(legend.position = 'right', axis.text.x = element_text(angle = 90)) +
  theme(text = element_text(size=12))

plot_sc_counts
```

![](report_files/figure-gfm/unnamed-chunk-3-1.png)<!-- -->

``` r
# Plotting these SC counts in the map of the Netherlands

# Some EU Contries
netherlands <- c( "Netherlands")
# Retrievethe map data
netherlands_map <- map_data("world", region = netherlands)

# Changing the coordinates of the isolate E8239 to meet the region from which the sample was isolated 

vana_isolates$Longitude[grep(pattern = 'E8239', x = vana_isolates$Strain)] <- 4.737694
vana_isolates$Latitude[grep(pattern = 'E8239', x = vana_isolates$Strain)] <- 52.62763

vana_isolates$Region <- ifelse(vana_isolates$Hospital %in% c('Antonius location Oudenrijn', 'Antonius location Nieuwegein','UMC Utrecht','Diakonessenhuis Utrecht','Meander Medisch Centrum','Zuwe Hofpoort'), 'Utrecht',
ifelse(vana_isolates$Hospital %in% c('Maasstad Rotterdam','Albert Schweitzer','Erasmus MC','LUMC','Westeinde','Leyenburg'), 'South-Holland',
ifelse(vana_isolates$Hospital %in% c('Viecuri MC Venlo','Orbis Medisch Centrum','Atrium','Laurentius Ziekenhuis Roermond','AZM'), 'Limburg',
ifelse(vana_isolates$Hospital %in% c('MC Zuiderzee','Flevoziekenhuis'), 'Flevoland',
ifelse(vana_isolates$Hospital %in% c('Isala klinieken Zwolle','MST'),'Overijsel',
ifelse(vana_isolates$Hospital %in% c('UMCG'),'Groningen',
ifelse(vana_isolates$Hospital %in% c('SLAZ','AMC','Westfries Gasthuis','OLVG, locatie oost','MC Alkmaar','Spaarne ziekenhuis'),'North-Holland',
ifelse(vana_isolates$Hospital %in% c('Rijnstate Arnhem','Gelderse Vallei'),'Gelderland',
ifelse(vana_isolates$Hospital %in% c('Admiraal de Ruyter ziekenhuis'),'Zeeland',
ifelse(vana_isolates$Hospital %in% c('Amphia ziekenhuis Breda'),'North-Brabant','unassigned'))))))))))

vana_isolates$Region_Longitude <- ifelse(vana_isolates$Region == 'Utrecht', 5.4,
                                 ifelse(vana_isolates$Region == 'South-Holland', 4.1,
                                 ifelse(vana_isolates$Region == 'Limburg', 6.4,
                                 ifelse(vana_isolates$Region == 'Flevoland', 5.471422,
                                 ifelse(vana_isolates$Region == 'Overijsel', 6.7, 
                                 ifelse(vana_isolates$Region == 'Groningen', 6.9,
                                 ifelse(vana_isolates$Region == 'North-Holland', 4.5, 
                                 ifelse(vana_isolates$Region == 'Gelderland', 6.2,
                                 ifelse(vana_isolates$Region == 'Zeeland', 3.9,
                                 ifelse(vana_isolates$Region == 'North-Brabant',5.1,NA))))))))))

vana_isolates$Region_Latitude <- ifelse(vana_isolates$Region == 'Utrecht', 52.092876,
                                 ifelse(vana_isolates$Region == 'South-Holland', 52.08,
                                 ifelse(vana_isolates$Region == 'Limburg', 51.3,
                                 ifelse(vana_isolates$Region == 'Flevoland', 52.7,
                                 ifelse(vana_isolates$Region == 'Overijsel', 52.5125, 
                                 ifelse(vana_isolates$Region == 'Groningen', 53.2,
                                 ifelse(vana_isolates$Region == 'North-Holland', 52.5, 
                                 ifelse(vana_isolates$Region == 'Gelderland', 52.1,
                                 ifelse(vana_isolates$Region == 'Zeeland', 51.5,
                                 ifelse(vana_isolates$Region == 'North-Brabant',51.55,NA))))))))))  

# We split the data based on year 



van_2012 <- subset(vana_isolates, vana_isolates$Year == 2012)
van_2013 <- subset(vana_isolates, vana_isolates$Year == 2013)
van_2014 <- subset(vana_isolates, vana_isolates$Year == 2014)
van_2015 <- subset(vana_isolates, vana_isolates$Year == 2015)

# Info regarding the coordinates of the regions

info_coordinates <- subset(vana_isolates, select = c('Region','Region_Longitude','Region_Latitude'))
info_coordinates <- info_coordinates[!duplicated(info_coordinates),]


# Map of the Netherlands 

NLD <- readRDS("../map/gadm36_NLD_1_sp.rds")

NLD <- subset(NLD, !NLD$NAME_1  %in% c("Zeeuwse meren", "IJsselmeer"))
```

    ## Loading required package: sp

    ## 
    ## Attaching package: 'sp'

    ## The following object is masked from 'package:scatterpie':
    ## 
    ##     recenter

``` r
NLD <- fortify(NLD)
```

    ## Regions defined for each Polygons

``` r
map <- ggplot(NLD) +
  theme_minimal()+
  geom_polygon( aes(x = long, y = lat, group = group),
  color = "gray50", fill = "gray90") +
  coord_map() 

# Preparing the data from 2012 

van_2012_filt <- van_2012 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0) %>%
  ungroup()

van_2012_filt <- subset(van_2012, van_2012$postBNGBAPS.2 %in% van_2012_filt$postBNGBAPS.2)

van_2012_filt <- van_2012_filt %>%
  group_by(Region, postBNGBAPS.2) %>%
  count() %>%
  ungroup()

van_2012_filt_spread <- van_2012_filt %>%
  spread(key = postBNGBAPS.2, value = n)

van_2012_filt_spread <- merge(van_2012_filt_spread, info_coordinates, by = 'Region')

van_2012_filt_spread[is.na(van_2012_filt_spread)] <- 0

van_2012_filt_spread$radius <- rowSums(van_2012_filt_spread[,c(2:13)]) 


# Preparing the data from 2013 

van_2013_filt <- van_2013 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0) %>%
  ungroup()

van_2013_filt <- subset(van_2013, van_2013$postBNGBAPS.2 %in% van_2013_filt$postBNGBAPS.2)

van_2013_filt <- van_2013_filt %>%
  group_by(Region, postBNGBAPS.2) %>%
  count() %>%
  ungroup()

van_2013_filt_spread <- van_2013_filt %>%
  spread(key = postBNGBAPS.2, value = n)

van_2013_filt_spread <- merge(van_2013_filt_spread, info_coordinates, by = 'Region')

van_2013_filt_spread[is.na(van_2013_filt_spread)] <- 0

van_2013_filt_spread$radius <- rowSums(van_2013_filt_spread[,c(2:9)]) 


# Preparing the data from 2014 

van_2014_filt <- van_2014 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0) %>%
  ungroup()

van_2014_filt <- subset(van_2014, van_2014$postBNGBAPS.2 %in% van_2014_filt$postBNGBAPS.2)

van_2014_filt <- van_2014_filt %>%
  group_by(Region, postBNGBAPS.2) %>%
  count() %>%
  ungroup()

van_2014_filt_spread <- van_2014_filt %>%
  spread(key = postBNGBAPS.2, value = n)

van_2014_filt_spread <- merge(van_2014_filt_spread, info_coordinates, by = 'Region')

van_2014_filt_spread[is.na(van_2014_filt_spread)] <- 0

van_2014_filt_spread$radius <- rowSums(van_2014_filt_spread[,c(2:10)]) 


#  Preparing the data from 2015 

van_2015_filt <- van_2015 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0) %>%
  ungroup()

van_2015_filt <- subset(van_2015, van_2015$postBNGBAPS.2 %in% van_2015_filt$postBNGBAPS.2)

van_2015_filt <- van_2015_filt %>%
  group_by(Region, postBNGBAPS.2) %>%
  count() %>%
  ungroup()

van_2015_filt_spread <- van_2015_filt %>%
  spread(key = postBNGBAPS.2, value = n)

van_2015_filt_spread <- merge(van_2015_filt_spread, info_coordinates, by = 'Region')

van_2015_filt_spread[is.na(van_2015_filt_spread)] <- 0

van_2015_filt_spread$radius <- rowSums(van_2015_filt_spread[,c(2:15)]) 


# We rename the dataframe to get all postBNGBAPS.2 with at least 1 isolate 

van_2012_filt <- van_2012 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0)

van_2012_filt <- subset(van_2012, van_2012$postBNGBAPS.2 %in% van_2012_filt$postBNGBAPS.2)

van_2013_filt <- van_2013 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0)

van_2013_filt <- subset(van_2013, van_2013$postBNGBAPS.2 %in% van_2013_filt$postBNGBAPS.2)

van_2014_filt <- van_2014 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0)

van_2014_filt <- subset(van_2014, van_2014$postBNGBAPS.2 %in% van_2014_filt$postBNGBAPS.2)

van_2015_filt <- van_2015 %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  filter(n > 0)

van_2015_filt <- subset(van_2015, van_2015$postBNGBAPS.2 %in% van_2015_filt$postBNGBAPS.2)

# Now we can finally plot the map of the netherlands 


plot_2012 <-  map +
  geom_jitter(data = van_2012_filt, width = 0.02, height = 0.02, alpha = 0.7, size = 1.5 , aes(x = van_2012_filt$Longitude, van_2012_filt$Latitude, color = as.factor(van_2012_filt$postBNGBAPS.2))) +
  theme(legend.position = 'none') +
  scale_color_manual(values=col_vector[sort(unique(van_2012_filt$postBNGBAPS.2))]) +
  annotate(geom="text", x= 5.104480, y= 52.092876, label="UT", color="black") +
  annotate(geom="text", x= 4.5, y= 52.08, label="SH", color="black") +
  annotate(geom="text", x= 5.9, y= 51.3, label="LI", color="black") +
  annotate(geom="text", x= 5.471422, y= 52.418536, label="FL", color="black") + 
  annotate(geom="text", x= 6.4, y= 52.5125, label="OV", color="black") + 
  annotate(geom="text", x= 6.56667, y= 53.3, label="GR", color="black") +
  annotate(geom="text", x= 4.8, y= 52.5, label="NH", color="black") + 
  annotate(geom="text", x= 5.91111, y= 52.1, label="GL", color="black") + 
  annotate(geom="text", x= 3.9, y= 51.5, label="ZE", color="black") + 
  annotate(geom="text", x= 5.1, y= 51.55, label="NB", color="black") + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) +
  geom_scatterpie(aes(x=Region_Longitude, y=Region_Latitude, group=Region),data=van_2012_filt_spread, cols=colnames(van_2012_filt_spread)[2:13], pie_scale = 2.0, alpha = 0.8) +
  scale_fill_manual(values=col_vector[sort(unique(van_2012_filt$postBNGBAPS.2))]) + theme(legend.position = 'None') + theme(plot.margin = unit(c(0, 0, 0, 0), "cm"))

plot_2013 <- map + 
  geom_jitter(data = van_2013_filt, width = 0.02, height = 0.02, alpha = 0.7, size = 1.5  , aes(x = van_2013_filt$Longitude,   van_2013_filt$Latitude, color = as.factor(van_2013_filt$postBNGBAPS.2))) +
  theme(legend.position = 'cornsilk') +
  scale_color_manual(values=col_vector[sort(unique(van_2013_filt$postBNGBAPS.2))]) +
  annotate(geom="text", x= 5.104480, y= 52.092876, label="UT", color="black") +
  annotate(geom="text", x= 4.5, y= 52.08, label="SH", color="black") +
  annotate(geom="text", x= 5.9, y= 51.3, label="LI", color="black") +
  annotate(geom="text", x= 5.471422, y= 52.418536, label="FL", color="black") + 
  annotate(geom="text", x= 6.4, y= 52.5125, label="OV", color="black") + 
  annotate(geom="text", x= 6.56667, y= 53.3, label="GR", color="black") +
  annotate(geom="text", x= 4.8, y= 52.5, label="NH", color="black") + 
  annotate(geom="text", x= 5.91111, y= 52.1, label="GL", color="black") + 
  annotate(geom="text", x= 3.9, y= 51.5, label="ZE", color="black") + 
  annotate(geom="text", x= 5.1, y= 51.55, label="NB", color="black") + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) +
  geom_scatterpie(aes(x=Region_Longitude, y=Region_Latitude, group=Region),data=van_2013_filt_spread, cols=colnames(van_2013_filt_spread)[2:9], pie_scale = 2.5, alpha = 0.8) +
  scale_fill_manual(values=col_vector[sort(unique(van_2013_filt$postBNGBAPS.2))]) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(legend.position = 'None')


plot_2014 <- map +
  geom_jitter(data = van_2014_filt, width = 0.02, height = 0.02, alpha = 0.7, size = 1.5  , aes(x = van_2014_filt$Longitude, van_2014_filt$Latitude, color = as.factor(van_2014_filt$postBNGBAPS.2)))  +
  theme(legend.position = 'none') +
  scale_color_manual(values=col_vector[sort(unique(van_2014_filt$postBNGBAPS.2))]) +
  annotate(geom="text", x= 5.104480, y= 52.092876, label="UT", color="black") +
  annotate(geom="text", x= 4.5, y= 52.08, label="SH", color="black") +
  annotate(geom="text", x= 5.9, y= 51.3, label="LI", color="black") +
  annotate(geom="text", x= 5.471422, y= 52.418536, label="FL", color="black") + 
  annotate(geom="text", x= 6.4, y= 52.5125, label="OV", color="black") + 
  annotate(geom="text", x= 6.56667, y= 53.3, label="GR", color="black") +
  annotate(geom="text", x= 4.8, y= 52.5, label="NH", color="black") + 
  annotate(geom="text", x= 5.91111, y= 52.1, label="GL", color="black") + 
  annotate(geom="text", x= 3.9, y= 51.5, label="ZE", color="black") + 
  annotate(geom="text", x= 5.1, y= 51.55, label="NB", color="black") + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) +
  geom_scatterpie(aes(x=Region_Longitude, y=Region_Latitude, group=Region),data=van_2014_filt_spread, cols=colnames(van_2014_filt_spread)[2:10], pie_scale = 2.5, alpha = 0.8) +
  scale_fill_manual(values=col_vector[sort(unique(van_2014_filt$postBNGBAPS.2))]) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(legend.position = 'None')


plot_2015 <- map +
  geom_jitter(data = van_2015_filt, width = 0.02, height = 0.02, alpha = 0.7, size = 1.5  , aes(x = van_2015_filt$Longitude,   van_2015_filt$Latitude, color = as.factor(van_2015_filt$postBNGBAPS.2))) +
  scale_color_manual(values=col_vector[sort(unique(van_2015_filt$postBNGBAPS.2))]) +
  theme(legend.position = 'none') + 
  annotate(geom="text", x= 5.104480, y= 52.092876, label="UT", color="black") +
  annotate(geom="text", x= 4.5, y= 52.08, label="SH", color="black") +
  annotate(geom="text", x= 5.9, y= 51.3, label="LI", color="black") +
  annotate(geom="text", x= 5.471422, y= 52.418536, label="FL", color="black") + 
  annotate(geom="text", x= 6.4, y= 52.5125, label="OV", color="black") + 
  annotate(geom="text", x= 6.56667, y= 53.3, label="GR", color="black") +
  annotate(geom="text", x= 4.8, y= 52.5, label="NH", color="black") + 
  annotate(geom="text", x= 5.91111, y= 52.1, label="GL", color="black") + 
  annotate(geom="text", x= 3.9, y= 51.5, label="ZE", color="black") + 
  annotate(geom="text", x= 5.1, y= 51.55, label="NB", color="black") + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) +
  geom_scatterpie(aes(x=Region_Longitude, y=Region_Latitude, group=Region),data=van_2015_filt_spread, cols=colnames(van_2015_filt_spread)[2:15], pie_scale = 2.5, alpha = 0.8) +
  scale_fill_manual(values=col_vector[sort(unique(van_2015_filt$postBNGBAPS.2))]) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(legend.position = 'None')

plot_grid(plot_2012, plot_2013, plot_2014, plot_2015, ncol = 2, nrow = 2, labels = c('2012','2013','2014','2015'))
```

![](report_files/figure-gfm/unnamed-chunk-4-1.png)<!-- -->

## Metrics of the SC

``` r
# Stats behind the vanA isolates

# Number of isolates considered in the study
nrow(vana_isolates)
```

    ## [1] 309

``` r
# Percentage of SCs 
vana_isolates %>%
  group_by(postBNGBAPS.2) %>%
  count() %>%
  mutate(percentage = n/nrow(vana_isolates)*100)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["postBNGBAPS.2"],"name":[1],"type":["int"],"align":["right"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]},{"label":["percentage"],"name":[3],"type":["dbl"],"align":["right"]}],"data":[{"1":"1","2":"17","3":"5.5016181"},{"1":"2","2":"11","3":"3.5598706"},{"1":"5","2":"1","3":"0.3236246"},{"1":"6","2":"4","3":"1.2944984"},{"1":"7","2":"2","3":"0.6472492"},{"1":"8","2":"5","3":"1.6181230"},{"1":"9","2":"1","3":"0.3236246"},{"1":"10","2":"27","3":"8.7378641"},{"1":"13","2":"102","3":"33.0097087"},{"1":"17","2":"52","3":"16.8284790"},{"1":"18","2":"42","3":"13.5922330"},{"1":"19","2":"2","3":"0.6472492"},{"1":"20","2":"17","3":"5.5016181"},{"1":"22","2":"11","3":"3.5598706"},{"1":"23","2":"2","3":"0.6472492"},{"1":"29","2":"6","3":"1.9417476"},{"1":"30","2":"4","3":"1.2944984"},{"1":"32","2":"3","3":"0.9708738"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Percentage of MLST types, not included in the manuscript but could be useful when comparing with other studies
vana_isolates %>%
  group_by(MLST) %>%
  count() %>%
  mutate(percentage = n/nrow(vana_isolates)*100)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["MLST"],"name":[1],"type":["int"],"align":["right"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]},{"label":["percentage"],"name":[3],"type":["dbl"],"align":["right"]}],"data":[{"1":"5","2":"2","3":"0.6472492"},{"1":"6","2":"5","3":"1.6181230"},{"1":"17","2":"5","3":"1.6181230"},{"1":"18","2":"27","3":"8.7378641"},{"1":"50","2":"4","3":"1.2944984"},{"1":"64","2":"1","3":"0.3236246"},{"1":"78","2":"6","3":"1.9417476"},{"1":"80","2":"29","3":"9.3851133"},{"1":"89","2":"3","3":"0.9708738"},{"1":"117","2":"27","3":"8.7378641"},{"1":"185","2":"1","3":"0.3236246"},{"1":"192","2":"2","3":"0.6472492"},{"1":"203","2":"103","3":"33.3333333"},{"1":"209","2":"1","3":"0.3236246"},{"1":"290","2":"52","3":"16.8284790"},{"1":"323","2":"2","3":"0.6472492"},{"1":"342","2":"1","3":"0.3236246"},{"1":"375","2":"6","3":"1.9417476"},{"1":"412","2":"2","3":"0.6472492"},{"1":"494","2":"11","3":"3.5598706"},{"1":"546","2":"1","3":"0.3236246"},{"1":"721","2":"2","3":"0.6472492"},{"1":"730","2":"2","3":"0.6472492"},{"1":"736","2":"2","3":"0.6472492"},{"1":"738","2":"1","3":"0.3236246"},{"1":"759","2":"1","3":"0.3236246"},{"1":"791","2":"2","3":"0.6472492"},{"1":"868","2":"1","3":"0.3236246"},{"1":"911","2":"2","3":"0.6472492"},{"1":"952","2":"1","3":"0.3236246"},{"1":"965","2":"1","3":"0.3236246"},{"1":"1007","2":"1","3":"0.3236246"},{"1":"1012","2":"1","3":"0.3236246"},{"1":"1177","2":"1","3":"0.3236246"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

# Whole-genome assemblies

## Availability

These isolates were previously whole-genome sequenced (Illumina
NextSeq). The raw reads are available through ENA Bioproject PRJEB28495
<https://www.ebi.ac.uk/ena/data/view/PRJEB28495>

## Trimming

The fastq reads were trimmed using trim\_galore using a phred score of
20

``` bash
# This part of the code was run on an HPC environment using a Snakemake pipeline 

trim_galore --paired --quality {params.phred} --basename {params.name} --output_dir {output.trim} {input.fw} {input.rv}
```

## Assembly

For the assemblies, we used Unicycler (normal mode) since gplas requires
of short-read graphs with a low amount of dead-ends. Unicycler uses
SPAdes to optimise the short-read graph obtained by trying different
k-mer
sizes

``` bash
# This part of the code was run on an HPC environment using a Snakemake pipeline 

unicycler -1 trimmed_reads/{params.name}_trimgalore/{params.name}_val_1.fq.gz -2 trimmed_reads/{params.name}_trimgalore/{params.name}_val_2.fq.gz --mode {params.mode} --keep 2 -o {output.unicycler_dir}
```

# Plasmid prediction

The plasmid prediction was performed using gplas (modularity value =
0.1, number of walks per seed = 50) in combination with mlplasmids
(Enterococcus faecium model). We consider as input the assembly graph
given by Unicycler in the chunk above

``` bash
# This part of the code was run on an HPC environment

qsub gplas.sh -i ../gplas_assemblies/short_read_assembly/"$strain"_unicycler/assembly.gfa -n $strain -q 0.1 -x 50 -c mlplasmids -s 'Enterococcus faecium'
```

The results of gplas consist in several fasta sequences containing the
bins of plasmid contigs which are interconnected in the plasmidome
network. These fasta bins were compared between each other using mash

## Pairwise distances between gplas bins

``` bash
# This part of the code was run on an HPC environment

mash sketch -k 21 -s 1000 -p 10 -o  reference.msh E*.fasta # E*.fasta refers to all the bins predicted by gplas in the vanA isolates 
mash dist reference.msh reference.msh -t -p 10 > mash_distances.txt 
```

## Network analysis of vanA plasmid bins

``` r
# Loading the mash distances 

distances <- read.table(file = '../mash/gplas_bin_comparison/mash_distances.txt')

# Parsing the dataframe 

bins <- distances[,1]
mash_df <- distances[,c(2:ncol(distances))]

# Cleaning the name of the bins   
bins <- gsub('.fasta','',bins)

# The first field (separated by underscore) correspond to the name of the isolates 
strains <- unique(str_split_fixed(pattern = '_', string = bins, n = 3)[,1])

# Setting the matrix correctly 
colnames(mash_df) <- bins
rownames(mash_df) <- bins

# Considering the object as a distance matrix 
dist_mash <- as.dist(mash_df)


# Using tidyverse to reestructure the matrix into a data frame 
plot_mash <- as.data.frame(mash_df) %>%
  gather(key = bin, value = mash_distance)

plot_mash$bin_versus <- bins

# Mini viz of the data frame 
head(plot_mash)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["bin"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mash_distance"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["bin_versus"],"name":[3],"type":["chr"],"align":["left"]}],"data":[{"1":"E0139_bin_1","2":"0.0000000","3":"E0139_bin_1","_rn_":"1"},{"1":"E0139_bin_1","2":"1.0000000","3":"E0139_bin_2","_rn_":"2"},{"1":"E0139_bin_1","2":"0.0171265","3":"E0595_bin_1","_rn_":"3"},{"1":"E0139_bin_1","2":"0.0277775","3":"E0656_bin_1","_rn_":"4"},{"1":"E0139_bin_1","2":"0.0659724","3":"E4239_bin_1","_rn_":"5"},{"1":"E0139_bin_1","2":"0.0628995","3":"E4239_bin_2","_rn_":"6"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Making sure we only include pairwise comparison of the bins 

plot_mash <- plot_mash[grep(pattern = '_bin|pl', x = plot_mash$bin),]
plot_mash <- plot_mash[grep(pattern = '_bin|pl', x = plot_mash$bin_versus),]

# We remove the comparison with the contigs which were unbinned by gplas, since in these cases gplas could not create plasmid-walks between different plasmid-predicted contigs

plot_mash <- plot_mash[- grep(pattern = '_Unbinned', x = plot_mash$bin),]
plot_mash <- plot_mash[- grep(pattern = '_Unbinned', x = plot_mash$bin_versus),]
```

The dataframe contained plasmids bins which also did not have the vanA
gene, thus we ran once again Abricate with the ResFinder database to
know which of the bins per isolate was the one bearing the vanA gene

``` bash
# This part of the code was run on an HPC environment

abricate --db resfinder --csv E*.fasta > abricate_results.csv 
```

The file created by Abricate allowed us to know which of the contigs
beared the vanA gene

``` r
# Loading abricate results 
abricate_resfinder <- read.csv(file = '../abricate/abricate_results.csv')

# Looking at the vanA gene
van_results <- subset(abricate_resfinder, abricate_resfinder$GENE == 'VanA_2')
van_bins <- van_results$X.FILE
van_bins <- gsub(pattern = '.fasta', replacement = '', x = van_bins) # van_bins is a vector with the names of the bins bearing the vanA gene 
```

Now we can continue with the
analysis

``` r
# Subsetting the dataframe with only pairwise distances between bins bearing the vanA gene

plot_mash <- subset(plot_mash, plot_mash$bin %in% van_bins)
plot_mash <- subset(plot_mash, plot_mash$bin_versus %in% van_bins)

# Removing possible pairwise duplicates
plot_mash <- plot_mash[!duplicated(plot_mash),]


# Removing pairwise connections of vanA bins against themselves (self-connections resulting in pairwise distances of 0.0)
plot_mash <- subset(plot_mash, plot_mash$bin != plot_mash$bin_versus)

# The name of the strains
strains <- unique(str_split_fixed(pattern = '_', string = bins, n = 3)[,1])



# Creating a new dataframe that will be later used in igraph. We create a column called weight, which corresponds to the Mash distance 

weight_graph <- data.frame(From_to = plot_mash$bin,
                           To_from = plot_mash$bin_versus,
                           weight = as.numeric(as.character(plot_mash$mash_distance)))

weight_graph$Strain <- str_split_fixed(string = weight_graph$From_to, pattern = '_', n = 2)[,1]
weight_graph$Pair_Strain <- str_split_fixed(string = weight_graph$To_from, pattern = '_', n = 2)[,1]


# Making sure we only consider strains present as part of this study, for example isolates from other years are discarded

isolates_to_discard <- setdiff(strains, vana_isolates$Strain)

weight_graph <- subset(weight_graph,! weight_graph$Strain %in% isolates_to_discard)
weight_graph <- subset(weight_graph,! weight_graph$Pair_Strain %in% isolates_to_discard)


threshold_distribution <- ggplot(weight_graph, aes(x=weight)) + 
    geom_histogram(aes(y=..density..),      # Histogram with density instead of count on y-axis
                   binwidth=.005,
                   colour="black", fill="white") +
    geom_vline(xintercept = 0.025, linetype = 'dashed') +
    geom_density(alpha=.5, fill="lightblue") + 
    labs(x = 'Mash Distance (k = 21, s = 1,000)')

vana_plasmids <- subset(vana_isolates, vana_isolates$Strain %in% strains)
```

This is how the distribution of pairwise distances betweens bins
containing the vanA gene looks like. We can observe that the threshold
(vertical dashed line) of 0.025 in the Mash Distance could be an optimal
cutoff since it splits the two modes observed in our binomial
distribution

``` r
threshold_distribution
```

![](report_files/figure-gfm/unnamed-chunk-14-1.png)<!-- -->

Now we choose to remove all the pairwise connections with a distance
higher than 0.025. This is the minimum distance that we consider to
establish that two vanA bins are actually similar

``` r
# Creating a new dataframe that we will use to subset the distances
filtered_graph <- weight_graph

# The R package igraph considers that lower weights correspond to a distances of 0, which is the opposite to the distance given by Mash
# To bypass this, we reverse the scale 0-1 to 1-0. 
filtered_graph$weight <- 1-filtered_graph$weight

# We keep only distances higher than 0.975
filtered_graph <- subset(filtered_graph, filtered_graph$weight > (1-0.025))

# Again, we make sure that we don't include self-connections
filtered_graph <- subset(filtered_graph, filtered_graph$From_to != filtered_graph$To_from)

# The column width will be considered by igraph to draw the width of the edges 
filtered_graph$width <- filtered_graph$weight

# We use the function from igraph to create a network from a dataframe
graph_pairs <- graph_from_data_frame(filtered_graph, directed = FALSE)

# We check that igraph considers the weight (reversed Mash distances) of the connections
is.weighted(graph_pairs)
```

    ## [1] TRUE

``` r
# We retrieve the name of the vertices in the network
vertices_graph <- V(graph_pairs)

# This is how the names look like
head(vertices_graph)
```

    ## + 6/270 vertices, named, from 2d8f4ae:
    ## [1] E7308_bin_3 E7309_bin_1 E7310_bin_1 E7311_bin_1 E7312_bin_2 E7313_bin_5

``` r
# We only keep the first part corresponding to 
isolates_graph <- str_split_fixed(string = names(vertices_graph), pattern = '_', n = 3)[,1]
  
isolates_graph <- data.frame(Strain = isolates_graph)

# We want to incorporate the network the information regarding the Sequencing Clusters (SC) which is given in the column postBNGBAPS2  
small_info <- subset(isolates_metadata, select = c('Strain','postBNGBAPS.2'))
color_info <- merge(isolates_graph, small_info, by = 'Strain')

# We assign the colours based on SC to the vertices in the network 
V(graph_pairs)$color <- col_vector[color_info$postBNGBAPS.2]

# We only keep the isolates name in the network 
V(graph_pairs)$name <- str_split_fixed(string = V(graph_pairs)$name, pattern = '_', n = 3)[,1]

# The size of the node is reduced so we can clearly see the structure of the graph

V(graph_pairs)$size <- 2.5
```

The orientation of the graph has some randomness, we kept and save the
coordinates of one possible orientation so we can always plot the
network in the same
way

``` r
# The orientation is the graph has some randomness, we try to fix it by fixing the seed 
set.seed(123)

par(mfrow=c(1,1), mar=c(1,1,1,1))

coords <- layout.fruchterman.reingold(graph_pairs) 

# To have always the same orientation in the plot graph, we stored the coorinates once 

#write.csv(x = coords, file = '/home/sergi/Data/dissemination_plasmids/coords_vanA_network.csv', row.names = FALSE)

# We recover the coordinates of the vertices 
coords <- read.csv(file = '../network/coords_vanA_network.csv')

coords$X <- NULL

coords <- as.matrix(coords)

# We plot the network 
plot(graph_pairs, layout = coords)

# We create a legend with the assignment of colours based on SC
text_legend <- sort(unique(color_info$postBNGBAPS.2))
colours_legend <- col_vector[text_legend]

legend("topleft", legend=paste('SC',text_legend), col = colours_legend , bty = "o", pch=20 , pt.cex = 3, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.5))
```

![](report_files/figure-gfm/unnamed-chunk-16-1.png)<!-- -->

The name of the vertices are important for the following steps but for
the visualization it is better if the vertices don’t have a name so we
avoid overlap between the names and the colour of the nodes can be
appreciated

``` r
# We store the name of the vertices in the network so we can put them again in the network 
storing_vertices_names <- V(graph_pairs)$name 
V(graph_pairs)$name <- ''

# We plot the network 
plot(graph_pairs, layout = coords)

# We create a legend with the assignment of colours based on SC
text_legend <- sort(unique(color_info$postBNGBAPS.2))
colours_legend <- col_vector[text_legend]

legend("topleft", legend=paste('SC',text_legend), col = colours_legend , bty = "o", pch=20 , pt.cex = 3, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.5))
```

![](report_files/figure-gfm/unnamed-chunk-17-1.png)<!-- -->

The network looks much better, and you can see the presence of different
components in the graph in which we find isolates from different SC
bearing similar plasmids

``` r
# We introduce again the name of the vertices in the graph 

V(graph_pairs)$name <- storing_vertices_names
```

Let’s inspect the network with more detail

``` r
graph_report <- data.frame(Strain = V(graph_pairs)$name, 
                           Component = as.numeric(as.character(components(graph_pairs)$membership)))

# Number of vertices in the graph 
nrow(graph_report)
```

    ## [1] 270

``` r
# Number of vertices per component, only components with > 2 nodes 
graph_report %>%
  group_by(Component) %>%
  count() %>%
  filter(n > 2) 
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Component"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"1","2":"11"},{"1":"2","2":"31"},{"1":"3","2":"144"},{"1":"6","2":"20"},{"1":"7","2":"17"},{"1":"8","2":"12"},{"1":"9","2":"4"},{"1":"13","2":"10"},{"1":"14","2":"7"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# The same information as before displayed in a different way 
components(graph_pairs)
```

    ## $membership
    ## E7308 E7309 E7310 E7311 E7312 E7313 E7321 E7323 E7324 E7325 E7326 E7327 
    ##     1     1     1     1     2     3     3     1     1     1     1     1 
    ## E7330 E7331 E7332 E7333 E7336 E7342 E7366 E7368 E7369 E7377 E7392 E7404 
    ##     3     3     4     3     5     3     3     3     3     3     3     6 
    ## E7405 E7413 E7414 E7415 E7416 E7417 E7418 E7419 E7432 E7470 E7474 E7475 
    ##     3     3     3     3     3     3     3     3     7     3     7     7 
    ## E7476 E7477 E7478 E7479 E7480 E7481 E7482 E7483 E7484 E7485 E7495 E7496 
    ##     3     3     6     3     3     3     3     3     3     3     3     3 
    ## E7497 E7498 E7499 E7500 E7501 E7502 E7503 E7504 E7513 E7516 E7519 E7520 
    ##     3     3     3     3     6     3     6     3     6     3     3     3 
    ## E7521 E7522 E7523 E7524 E7525 E7526 E7527 E7528 E7529 E7530 E7531 E7535 
    ##     6     3     3     3     3     6     3     3     3     3     6     3 
    ## E7536 E7537 E7538 E7539 E7543 E7544 E7545 E7546 E7547 E7548 E7549 E7550 
    ##     3     6     6     3     6     3     6     8     3     3     3     3 
    ## E7551 E7554 E7555 E7556 E7573 E7575 E7576 E7577 E7578 E7579 E7580 E7581 
    ##     6     3     6     9     2     3     6     4     3     3     8     3 
    ## E7585 E7586 E7587 E7588 E7595 E7596 E7603 E7608 E7609 E7641 E7672 E7673 
    ##     6     3     3     3     3     7     2     3     8     3     2     2 
    ## E7678 E7684 E7685 E7688 E7696 E7697 E7698 E7699 E7700 E7703 E7836 E7837 
    ##     9     2     8     2     3     3     3     3     2     2     3     1 
    ## E7850 E7858 E7876 E7877 E7882 E7884 E7885 E7886 E7932 E7941 E7963 E7964 
    ##     6     6     3     3     2     3     3     3     3     3     3     3 
    ## E7965 E7966 E7967 E7968 E7969 E7986 E7987 E7989 E7990 E7991 E7992 E7993 
    ##     3     3     3     3     3     3     3     3     3     3     3     2 
    ## E7994 E7995 E7996 E7999 E8001 E8002 E8003 E8004 E8008 E8009 E8010 E8011 
    ##     2     2     2     3     3     2     3     3     3     2     8     3 
    ## E8012 E8013 E8014 E8015 E8016 E8017 E8018 E8020 E8026 E8028 E8033 E8034 
    ##     2     2     2     3     3     3     3     6     7     2     7    10 
    ## E8035 E8039 E8041 E8042 E8044 E8045 E8046 E8048 E8049 E8057 E8128 E8129 
    ##     3     7     7     7     6     3     1    11     7    12    11     3 
    ## E8150 E8155 E8156 E8157 E8158 E8164 E8167 E8168 E8170 E8172 E8173 E8174 
    ##     7    13    13    13    13    13    13    13    13     7     3    13 
    ## E8175 E8176 E8177 E8180 E8182 E8184 E8188 E8189 E8191 E8192 E8193 E8201 
    ##    13     8     8    14     3     3     2     2     3     3     3     2 
    ## E8202 E8205 E8209 E8210 E8223 E8224 E8233 E8237 E8303 E8321 E8323 E8339 
    ##     5     2    12     2     3     2     9     3    15     3     9     3 
    ## E8340 E8341 E8343 E8344 E8345 E8346 E8347 E8348 E8349 E8350 E8351 E8386 
    ##     3     3     3     3     3     3     3     3     3     3     3     3 
    ## E8387 E8388 E8390 E8391 E8392 E8393 E8394 E8395 E8398 E8399 E8402 E8403 
    ##     3     2     3    15     8     3     3     3    16     3     3     8 
    ## E8410 E8415 E8416 E8421 E8423 E8427 E8431 E8437 E8438 E8439 E8441 E8442 
    ##    16     3     3     3     3     7     3     2     2     7     7     3 
    ## E8444 E8447 E8448 E8449 E8450 E8454 E8455 E8459 E9000 E9002 E9005 E9006 
    ##     3    14    14     3     8    10     7    14     2    14    14     7 
    ## E9007 E9009 E9010 E9015 E9016 E9017 
    ##     3     3     8     8    14     2 
    ## 
    ## $csize
    ##  [1]  11  31 144   2   2  20  17  12   4   2   2   2  10   7   2   2
    ## 
    ## $no
    ## [1] 16

``` r
which(components(graph_pairs)$csize > 10)
```

    ## [1] 1 2 3 6 7 8

It is clear that the component 3 has a high number of nodes (vertices)
but we can also observe the presence of subcomponents which are highly
interconnected and with fewer connections to the rest of the component.
For ths reason, we decided to compute the modularity values of the
component

``` r
dg <- decompose.graph(graph_pairs) # returns a list of three graphs

# dg is the object in which the different components in the graph are considered as subgraphs 

# We consider the 3rd component with a total of 144 vertices

graph_interest <- dg[[3]]

# This is the graph if we only plot that particular component 
plot(graph_interest)
```

![](report_files/figure-gfm/unnamed-chunk-20-1.png)<!-- -->

``` r
# Partitioning the subgraph into different clusters using the Louvain method 

graph_louvain <- cluster_louvain(graph_interest)
modularity(graph_louvain)
```

    ## [1] 0.4254874

``` r
# The component is split into 3 clusters 

info_louvain <- data.frame(Cluster = graph_louvain$membership,
                            Strain = graph_louvain$names)

info_louvain %>%
  group_by(Cluster) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Cluster"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"1","2":"6"},{"1":"2","2":"76"},{"1":"3","2":"62"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

Now we are going to consider the clusters from component 3 as
independent and recover all the vertices from the other components
present in the original
graph

``` r
# We merge the information regarding the original component, postBNGBAPS.2 with the cluster assignment given by Louvain method 
graph_report_info <- merge(graph_report, info_louvain, by = 'Strain')

# We also need to consider the other vertices present in other components in the original graph 
other_components <- subset(graph_report,! graph_report$Strain %in% graph_report_info$Strain)
other_components$Cluster <- NA # These components weren't further split into more clusters 

# We put all together in the same dataframe 
graph_report_info <- rbind(graph_report_info, other_components)

# This is the final components/clusters that we consider 
graph_report_info %>%
  group_by(Component, Cluster) %>%
  count() %>%
  filter(n > 10)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Component"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["Cluster"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"1","2":"NA","3":"11"},{"1":"2","2":"NA","3":"31"},{"1":"3","2":"2","3":"76"},{"1":"3","2":"3","3":"62"},{"1":"6","2":"NA","3":"20"},{"1":"7","2":"NA","3":"17"},{"1":"8","2":"NA","3":"12"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# We also integrate information of the isolates regarding the hospital from which they were isolated, isolation time... 
meta_graph_report <- merge(graph_report_info, vana_isolates, by = 'Strain')
```

The clusters below were considered as vanA plasmid clusters

``` r
meta_graph_report %>%
  group_by(Component, Cluster) %>%
  count() %>%
  filter(n >= 10)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Component"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["Cluster"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"1","2":"NA","3":"11"},{"1":"2","2":"NA","3":"31"},{"1":"3","2":"2","3":"76"},{"1":"3","2":"3","3":"62"},{"1":"6","2":"NA","3":"20"},{"1":"7","2":"NA","3":"17"},{"1":"8","2":"NA","3":"12"},{"1":"13","2":"NA","3":"10"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
combination_vana_isolates <- meta_graph_report

# These components/clusters were renamed as vanA clusters 

combination_vana_isolates$vanA_cluster <- ifelse(combination_vana_isolates$Component == 1, 1,
                                        ifelse(combination_vana_isolates$Component == 2, 2,
                                        ifelse(combination_vana_isolates$Component == 6, 5,
                                        ifelse(combination_vana_isolates$Component == 7, 6,
                                        ifelse(combination_vana_isolates$Component == 8, 7,
                                        ifelse(combination_vana_isolates$Component == 13, 8, 
                                        ifelse(combination_vana_isolates$Cluster == 2, 3,
                                        ifelse(combination_vana_isolates$Cluster == 3, 4, NA))))))))

# We create this column so later we can upload the data into microreact and integrate it with PopPUNK 
combination_vana_isolates$id <- combination_vana_isolates$Strain


graph_representation <- graph_pairs

node_names <- V(graph_representation)$name

# We colour the plasmid bins based on the colours that we will use in the rest of the figures 

V(graph_representation)$color <- ifelse(combination_vana_isolates$vanA_cluster == 1, 'orange',
ifelse(combination_vana_isolates$vanA_cluster == 2, pal[11],
ifelse(combination_vana_isolates$vanA_cluster == 3, pal[6],
ifelse(combination_vana_isolates$vanA_cluster == 4, pal[5],
ifelse(combination_vana_isolates$vanA_cluster == 5, pal[10], 
ifelse(combination_vana_isolates$vanA_cluster == 6, pal[7],
ifelse(combination_vana_isolates$vanA_cluster == 7, pal[1],
ifelse(combination_vana_isolates$vanA_cluster == 8, pal[9],'gray'))))))))


V(graph_representation)$color[which(is.na(V(graph_representation)$color) == TRUE)] <- 'gray'

length(node_names) #270 nodes are present in the network 
```

    ## [1] 270

``` r
V(graph_representation)$name <- ''

plot(graph_representation, layout = coords)

# We create a legend with the assignment of colours based on SC
text_legend <- sort(unique(combination_vana_isolates$vanA_cluster))
text_legend <- c(text_legend,'(Unassigned)')
colours_legend <- c('orange',pal[11],pal[6],pal[5],pal[10],pal[7],pal[1],pal[9],'gray')

legend("topleft", legend=paste('Bin Group',text_legend), col = colours_legend , bty = "o", pch=20 , pt.cex = 3, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.0))
```

![](report_files/figure-gfm/unnamed-chunk-22-1.png)<!-- -->

# PopPUNK analysis

We ran PopPUNK to obtain a core-genome based neighbour-joinning tree of
the vanA isolates present in the collection.

``` bash
# We ran this on an HPC environment 

poppunk --easy-run --r-files clean_vana_list.txt --output vana_poppunk --threads 8 --plot-fit 5 --min-k 13 --full-db --microreact
```

Now we can further analyse the results from
PopPUNK

``` r
# We load the information given by PopPUNK and than we will later introduce in Microreact as well 

poppunk_meta <- read.csv(file = '../poppunk/vrefm_poppunk/vrefm_poppunk_microreact_clusters.csv')

# Six isolates were not included in the PopPUNK analysis since we did not get a short-read assembly for those 

setdiff(vana_isolates$Strain, poppunk_meta$id)
```

    ## [1] "E7931" "E7942" "E8155" "E8387" "E8437" "E9000"

``` r
# Thus, the core-genome tree generated by PopPUNK only consisted of 303 isolates 
nrow(poppunk_meta)
```

    ## [1] 303

``` r
#write.csv(x = selection_poppunk, file = '../poppunk/vrefm_poppunk/afterrevisions_complete_metadata.csv')
```

# Microreact link

All the metadata and information of the collection can be easily
accessed in Microreact.

``` bash
# https://microreact.org/project/mfEYIjcuu
```

# Core-genome tree + Metadata

PopPUNK also provides a core-genome tree. That core-genome tree was also
given in the microreact project. We use the R package ggtree to
illustrate the position of the isolates in the tree together with SC,
PopPUNK cluster and vanA cluster assignments. This is relevant to
observe if isolates distant in the tree actually are part of the same
vanA cluster. This would indicate that non-clonal isolates shared the
same vanA plasmid.

``` r
# Loading the core-genome tree 

poppunk_tree <- read.tree('../poppunk/vrefm_poppunk/vrefm_poppunk_core_NJ.nwk')

# Using ggtree to display the core-genome 
ggtree_bionj <- ggtree(poppunk_tree, layout = 'rectangular') + geom_treescale()

# This is how the core-genome tree looks like 
ggtree_bionj
```

![](report_files/figure-gfm/unnamed-chunk-26-1.png)<!-- -->

``` r
# Merging the information from PopPUNK together with the vanA cluster assignments 
collection_poppunk <- merge(poppunk_meta, combination_vana_isolates, by = 'id')

# This dataframe compares the PopPUNK cluster assignment with the SC 
pop_baps <- collection_poppunk %>%
  group_by(combined_Cluster__autocolour, postBNGBAPS.2) %>% 
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))

head(pop_baps)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["combined_Cluster__autocolour"],"name":[1],"type":["int"],"align":["right"]},{"label":["postBNGBAPS.2"],"name":[2],"type":["int"],"align":["right"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]},{"label":["freq"],"name":[4],"type":["dbl"],"align":["right"]},{"label":["round"],"name":[5],"type":["dbl"],"align":["right"]}],"data":[{"1":"1","2":"2","3":"2","4":"0.01923077","5":"1.9"},{"1":"1","2":"10","3":"9","4":"0.08653846","5":"8.7"},{"1":"1","2":"13","3":"93","4":"0.89423077","5":"89.4"},{"1":"2","2":"17","3":"49","4":"0.98000000","5":"98.0"},{"1":"2","2":"23","3":"1","4":"0.02000000","5":"2.0"},{"1":"3","2":"1","3":"10","4":"1.00000000","5":"100.0"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Now we parse the data to fit into the Microreact project 
collection_poppunk$Year <- NULL

collection_poppunk$year <- str_split_fixed(string = collection_poppunk$Isolation.date, pattern = '-', n = 3)[,1]
collection_poppunk$month <- str_split_fixed(string = collection_poppunk$Isolation.date, pattern = '-', n = 3)[,2]
collection_poppunk$day <- str_split_fixed(string = collection_poppunk$Isolation.date, pattern = '-', n = 3)[,3]

colnames(collection_poppunk)[8] <- 'SC'
colnames(collection_poppunk)[2] <- 'PopPUNK_cluster'

collection_poppunk$SC__colour <- col_vector[collection_poppunk$SC]

selection_poppunk <- subset(collection_poppunk, select = c('id','SC','SC__colour','PopPUNK_cluster','vanA_cluster','Hospital','Latitude','Longitude','year','month','day'))

colnames(selection_poppunk)[5] <- 'predicted_vanA_plasmid_type'

selection_poppunk$Plasmid_type <- ifelse(selection_poppunk$predicted_vanA_plasmid_type == 1, 'I',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 2, 'C',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 3, 'C',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 4, 'B',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 5, 'J',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 6, 'D',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 7, 'A',
ifelse(selection_poppunk$predicted_vanA_plasmid_type == 8, 'K',NA))))))))

# We generate a metadata file that we can upload to microreact

selection_poppunk$predicted_vanA_plasmid_type <- NULL

selection_poppunk <- subset(selection_poppunk, select = c('id','SC','SC__colour','PopPUNK_cluster','Plasmid_type','Hospital','Latitude','Longitude','year','month','day'))

# The following steps are required to combine the metadata with the core-genome tree 

labels_tree <- ggtree_bionj$data$label
  
info_strains <- collection_poppunk[collection_poppunk$Strain %in% labels_tree,]
  
tree_add_info <- data.frame(label = as.character(info_strains$Strain),
             Poppunk_cluster = info_strains$PopPUNK_cluster,
             vanA_cluster = info_strains$vanA_cluster,
             SC = info_strains$SC,
             Strain = info_strains$Strain)

tree_add_info$Plasmid_type <- ifelse(tree_add_info$vanA_cluster == 1, 7,
ifelse(tree_add_info$vanA_cluster == 2, 3,
ifelse(tree_add_info$vanA_cluster == 3, 3,
ifelse(tree_add_info$vanA_cluster == 4, 2,
ifelse(tree_add_info$vanA_cluster == 5, 8,
ifelse(tree_add_info$vanA_cluster == 6, 4,
ifelse(tree_add_info$vanA_cluster == 7, 1,
ifelse(tree_add_info$vanA_cluster == 8, 9,NA))))))))

tree_add_info <- subset(tree_add_info, tree_add_info$Plasmid_type %in% c(1,2,3,4,7,8,9))
```

# Stats of vanA clusters (SC, PopPUNK clusters)

``` r
vanA_baps <- tree_add_info %>%
  group_by(vanA_cluster, SC) %>%
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))

baps_vanA_cluster <- tree_add_info %>%
  group_by(SC, vanA_cluster) %>%
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))


vanA_punk <- tree_add_info %>%
  group_by(vanA_cluster, Poppunk_cluster) %>%
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))
```

# TETyper

We hypothesized that isolates from the same vanA cluster should bear
similar Tn1546 transposon variants, at least if the transmission between
them occurred recently. For this Tn1546 analysis, we used TETyper using
the Tn1546 original variant (Arthur et
al. 1993)

``` bash
TETyper.py --ref ../reference_type.fasta --fq1 ../trimmed_reads/"$isolate"_trimgalore/"$isolate"_val_1.fq.gz --fq2 ../trimmed_reads/"$isolate"_trimgalore/"$isolate"_val_2.fq.gz --outprefix "$isolate"_test --flank_len 5
```

We combined all the results into a single text file

``` r
# Loading the file 
tetyper_results <- read.table(file = '../transposons/tetyper_results.txt')

# Parsing the file 
tetyper_results <- subset(tetyper_results, tetyper_results$V2 != 'Deletions')
colnames(tetyper_results) <- c('Strain','Deletions','Structural_variant','SNPs_homo','SNPs_hetero','Hetero_snps_counts','SNP_variant','Combined_variant','Left_flank', 'Right_flank','Left_flank_counts','Right_flank_counts')

head(tetyper_results)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["Strain"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["Deletions"],"name":[2],"type":["fctr"],"align":["left"]},{"label":["Structural_variant"],"name":[3],"type":["fctr"],"align":["left"]},{"label":["SNPs_homo"],"name":[4],"type":["fctr"],"align":["left"]},{"label":["SNPs_hetero"],"name":[5],"type":["fctr"],"align":["left"]},{"label":["Hetero_snps_counts"],"name":[6],"type":["fctr"],"align":["left"]},{"label":["SNP_variant"],"name":[7],"type":["fctr"],"align":["left"]},{"label":["Combined_variant"],"name":[8],"type":["fctr"],"align":["left"]},{"label":["Left_flank"],"name":[9],"type":["fctr"],"align":["left"]},{"label":["Right_flank"],"name":[10],"type":["fctr"],"align":["left"]},{"label":["Left_flank_counts"],"name":[11],"type":["fctr"],"align":["left"]},{"label":["Right_flank_counts"],"name":[12],"type":["fctr"],"align":["left"]}],"data":[{"1":"E7308","2":"none","3":"unknown","4":"G7747T|C8833T","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"TATTC","10":"TATTC","11":"91","12":"96","_rn_":"2"},{"1":"E7309","2":"none","3":"unknown","4":"G7747T|C8833T","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"TATTC","10":"TATTC","11":"88","12":"137","_rn_":"4"},{"1":"E7310","2":"none","3":"unknown","4":"G7747T|C8833T","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"TATTC","10":"TATTC","11":"64","12":"108","_rn_":"6"},{"1":"E7311","2":"none","3":"unknown","4":"G7747T|C8833T","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"TATTC","10":"TATTC","11":"83","12":"167","_rn_":"8"},{"1":"E7312","2":"1-3417|8650-8827","3":"unknown","4":"none","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"none","10":"CACTA","11":"none","12":"112","_rn_":"10"},{"1":"E7313","2":"1-3417|8650-8827","3":"unknown","4":"none","5":"none","6":"none","7":"unknown","8":"unknown-unknown","9":"none","10":"CACTA","11":"none","12":"125","_rn_":"12"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
tetyper_results$Combi <- paste(tetyper_results$Deletions, tetyper_results$SNPs_homo, sep = '_')
tetyper_results$Combi <- paste(tetyper_results$Combi, tetyper_results$SNPs_hetero, sep = '_')

tn_variants <- tetyper_results
tn_variants$Strain <- NULL
tn_variants <- tn_variants[,c(1:7)]
tn_variants <- tn_variants[! duplicated(tn_variants),]

# We save the variants so they are available for the readers as well 

#write.csv(x = tn_variants, file = '../transposons/results_tetyper.csv')

# In our set of isolates, we identified the different variants using an ad-hoc scheme. Basically each SNP and deletion are assigned with one letter or number. That facilitates the comparison of variants which are similar between each other, e.g. incorporation of a SNP  

variants_annotated <- read.csv(file = '../transposons/variants_annotated.csv')

variants_annotated$Combi <- paste(variants_annotated$Deletions, variants_annotated$SNPs_homo, sep = '_')
variants_annotated$Combi <- paste(variants_annotated$Combi, variants_annotated$SNPs_hetero, sep = '_')

# We merge the annotation of the variants with the results from tetpyer

annotation_tn <- merge(tetyper_results, variants_annotated, by = 'Combi')

#write.csv(x = annotation_tn, file = '/home/sergi/Data/dissemination_plasmids/figures/neat/v3/Suppl_Table_S2.csv')

# We incorporate the information that we used in ggtree, so we have a combo between SC, PopPUNK cluster and vanA cluster 

annotation_tn_meta <- merge(annotation_tn, tree_add_info, by = 'Strain')


# In the end, we have 225 isolates for which we have complete metadata information, hierBAPS SC, plasmid type and Tn1546 assignment 

nrow(annotation_tn_meta)
```

    ## [1] 225

``` r
report_tn <- annotation_tn_meta %>%
  group_by(vanA_cluster, Tn_variant) %>%
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))

detailed_tn <- annotation_tn_meta %>%
  group_by(vanA_cluster, Tn_variant, Combi) %>%
  summarise(n = n()) %>%
  mutate(freq = n / sum(n)) %>%
  mutate(round = round(freq*100,1))

# We plot the variants that we found depending on the vanA cluster from which the isolate was assigned 
partition_high <- subset(report_tn, report_tn$vanA_cluster %in% c(1,2,3,4,5,6,7,8))

partition_high$vanA_cluster <- paste('Predicted pl. type', partition_high$vanA_cluster)

ggplot(partition_high, aes(x = Tn_variant, y = freq, fill = Tn_variant)) + geom_col() + facet_wrap(~ vanA_cluster, ncol = 2, nrow = 4) + theme_bw() + theme(axis.text.x = element_text(angle = 45, hjust = 1)) + labs(x = '', y = 'Frequency',fill = 'Tn1546 variant') + scale_fill_hue(l=50, c=85) 
```

![](report_files/figure-gfm/unnamed-chunk-29-1.png)<!-- -->

``` r
annotation_tn_meta$Transposon_number <- ifelse(annotation_tn_meta$Tn_variant == '25II', 1,
                                        ifelse(annotation_tn_meta$Tn_variant == '3468MN', 2,
                                        ifelse(annotation_tn_meta$Tn_variant == '46', 3,
                                        ifelse(annotation_tn_meta$Tn_variant == '468MN', 4, 
                                        ifelse(annotation_tn_meta$Tn_variant == '46M', 5, 
                                        ifelse(annotation_tn_meta$Tn_variant == '46MN', 6,
                                        ifelse(annotation_tn_meta$Tn_variant == '5II', 7,
                                        ifelse(annotation_tn_meta$Tn_variant == '5MIYZ', 8, 
                                        ifelse(annotation_tn_meta$Tn_variant == '6M', 9,
                                        ifelse(annotation_tn_meta$Tn_variant == 'MNI', 10,
                                        ifelse(annotation_tn_meta$Tn_variant == 'MNII', 11,
                                        ifelse(annotation_tn_meta$Tn_variant == 'MNIYZ', 12,
                                        ifelse(annotation_tn_meta$Tn_variant == 'MZ', 13,
                                        ifelse(annotation_tn_meta$Tn_variant == 'Original', 14, 15))))))))))))))

annotation_tn_meta %>%
  group_by(Plasmid_type) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Plasmid_type"],"name":[1],"type":["dbl"],"align":["right"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"1","2":"11"},{"1":"2","2":"62"},{"1":"3","2":"96"},{"1":"4","2":"17"},{"1":"7","2":"11"},{"1":"8","2":"19"},{"1":"9","2":"9"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
info_plot <- select(annotation_tn_meta, c('Strain','SC','Plasmid_type','Tn_variant'))

info_plot$Plasmid_variant <- ifelse(info_plot$Plasmid_type == 1, 'A',
                             ifelse(info_plot$Plasmid_type == 2, 'B',
                             ifelse(info_plot$Plasmid_type == 3, 'C',
                             ifelse(info_plot$Plasmid_type == 4, 'D',
                             ifelse(info_plot$Plasmid_type == 7, 'J',
                             ifelse(info_plot$Plasmid_type == 8, 'I',
                             ifelse(info_plot$Plasmid_type == 9, 'E','Unassigned')))))))


info_plasmid_plot <- info_plot

info_tn_plot <- info_plot

info_plasmid_plot$SC <- NULL
info_plasmid_plot$Plasmid_type <- NULL
rownames(info_plasmid_plot) <- info_plasmid_plot$Strain
info_plasmid_plot$Strain <- NULL
info_plasmid_plot$Tn_variant <- NULL

info_tn_plot$SC <- NULL
info_tn_plot$Plasmid_type <- NULL
rownames(info_tn_plot) <- info_tn_plot$Strain
info_tn_plot$Strain <- NULL
info_tn_plot$Plasmid_variant <- NULL


sc_tree <- ggtree_bionj %<+% info_plot + geom_tippoint(aes(color = as.factor(SC)), size = 1.5) + scale_color_manual(values = col_vector[sort(unique(annotation_tn_meta$SC))]) # For 78 points we don't have the entire information related to Plasmid Type or Transposon Variant 

# Plasmid tree 

colnames(info_plasmid_plot) <- c('Plasmid')

plasmid_tree <- gheatmap(sc_tree, info_plasmid_plot, width=0.1, colnames_angle = 0) + scale_fill_manual(values=c(pal[1],pal[5],pal[6],pal[7],pal[9],pal[10],'orange'))
```

    ## Scale for 'fill' is already present. Adding another scale for 'fill',
    ## which will replace the existing scale.

``` r
# Transposon tree

qual_col_pals <- brewer.pal.info[brewer.pal.info$category == 'qual',]
distinct_col_vector <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

colnames(info_tn_plot) <- c('Transposon')
tn_tree <- gheatmap(sc_tree, info_tn_plot, width=0.1, colnames_angle = 0) + scale_fill_manual(values = distinct_col_vector)
```

    ## Scale for 'fill' is already present. Adding another scale for 'fill',
    ## which will replace the existing scale.

``` r
plot_grid(plasmid_tree, tn_tree) 
```

    ## Warning: Removed 78 rows containing missing values (geom_point_g_gtree).
    
    ## Warning: Removed 78 rows containing missing values (geom_point_g_gtree).

![](report_files/figure-gfm/unnamed-chunk-29-2.png)<!-- -->

# Metadata incorporated in microreact

The ID of the isolate, its hierBAPS SC, plasmid type and Tn1546
assignment must be available for the reader in
microreact

``` r
# Some of the isolates in our network corresponded to singletons or were in small components that were not considered as vanA clusters. These isolates were also part of PopPUNK analysis 
singletons <- subset(vana_isolates, vana_isolates$Strain %in% setdiff(poppunk_meta$id, collection_poppunk$id))
singletons$id <- singletons$Strain

# 37 isolates were not assigned with a particular plasmid bin group 
nrow(singletons)
```

    ## [1] 37

``` r
# We also have some isolates for which we don't have a Tn1546 variant assigned from TETyper 
length(setdiff(poppunk_meta$id, annotation_tn$Strain))
```

    ## [1] 13

``` r
singletons_but_tn <- subset(annotation_tn, annotation_tn$Strain %in% singletons$Strain)
singletons_but_tn_info <- subset(singletons_but_tn, select = c('Strain','Combi','Tn_variant'))
singletons_but_tn_info$Plasmid_type <- 'Unassigned'

no_plasmid_no_tn <- setdiff(singletons$Strain, singletons_but_tn$Strain)

no_plasmid_no_tn_info <- data.frame(Strain = no_plasmid_no_tn,
           Combi = 'Unassigned',
           Tn_variant = 'Unassigned',
           Plasmid_type = 'Unassigned')

singletons_all_info <- rbind(singletons_but_tn_info, no_plasmid_no_tn_info)

plasmid_assignment_info <- subset(collection_poppunk, select = c('Strain','vanA_cluster'))
plasmid_tn_assignment_info <- merge(plasmid_assignment_info, annotation_tn, by = 'Strain')

plasmid_no_tn <- subset(collection_poppunk, collection_poppunk$Strain %in% setdiff(plasmid_assignment_info$Strain, plasmid_tn_assignment_info$Strain))

plasmid_no_tn <- subset(plasmid_no_tn, select = c('Strain','vanA_cluster'))
plasmid_no_tn$Combi <- 'Unassigned'
plasmid_no_tn$Tn_variant <- 'Unassigned'

colnames(plasmid_no_tn)[2] <- 'Plasmid_type'
plasmid_no_tn <- subset(plasmid_no_tn, select = c('Strain','Combi','Tn_variant','Plasmid_type'))


plasmid_tn_assignment_info <- subset(plasmid_tn_assignment_info, select = c('Strain','Combi','Tn_variant','vanA_cluster'))
colnames(plasmid_tn_assignment_info)[4] <- 'Plasmid_type'

all_info <- rbind(plasmid_tn_assignment_info, singletons_all_info)
all_info <- rbind(all_info, plasmid_no_tn)

all_info$Combi <- gsub(pattern = '_none', replacement = '', x = all_info$Combi)
all_info$Combi <- gsub(pattern = 'none_', replacement = '', x = all_info$Combi)

all_info$Plasmid_type <- ifelse(all_info$Plasmid_type == 1, 'J',
ifelse(all_info$Plasmid_type == 2, 'C',
ifelse(all_info$Plasmid_type == 3, 'C',
ifelse(all_info$Plasmid_type == 4, 'B',
ifelse(all_info$Plasmid_type == 5, 'I',
ifelse(all_info$Plasmid_type == 6, 'D',
ifelse(all_info$Plasmid_type == 7, 'A',
ifelse(all_info$Plasmid_type == 8, 'E','Unassigned'))))))))


all_info$Plasmid_type[is.na(all_info$Plasmid_type)] <- "Unassigned"

sc_strain <- subset(vana_isolates, select = c('postBNGBAPS.2','Strain','Isolation.date','Latitude','Longitude','Hospital'))
all_info <-merge(all_info, sc_strain, by = 'Strain')
colnames(all_info)[c(1,2,5)] <- c('id','Tn1546_description(Deletions+SNPs)','SC')

all_info$year <- str_split_fixed(string = all_info$Isolation.date, pattern = '-', n = 3)[,1]
all_info$month <- str_split_fixed(string = all_info$Isolation.date, pattern = '-', n = 3)[,2]
all_info$day <- str_split_fixed(string = all_info$Isolation.date, pattern = '-', n = 3)[,3]


all_info$SC__colour <- col_vector[all_info$SC]

all_info$Hospital <- gsub(pattern = ',', replacement = '', x = all_info$Hospital)

all_info$`Tn1546_description(Deletions+SNPs)` <- gsub(pattern = '\\|', replacement = '+', x = all_info$`Tn1546_description(Deletions+SNPs)`)
all_info$`Tn1546_description(Deletions+SNPs)` <- gsub(pattern = '_', replacement = '+', x = all_info$`Tn1546_description(Deletions+SNPs)`)

#write.csv(x = all_info, file = '/home/sergi/Data/dissemination_plasmids/poppunk/vrefm_poppunk/second_rebuttal_micro_metadata.csv', quote = FALSE)
```

# Network of complete vanA plasmid sequences + and PLSDB sequences

From a previous study, we had available 26 complete plasmid sequences
bearing the vanA gene. We also had information regarding isolation date,
isolation country, SC… They were a perfect set to combine with our
prediction. We have also included a set of 60 complete plasmid sequences
from the PLSDB database that carried the Tn1546 sequence.

We perform the same analysis as we did with the bins predicted by gplas.
But in this case, we are working with complete genomes.

## Pairwise distances between complete plasmid sequences

``` bash

mash sketch -k 21 -s 1000 -p 10 -o  reference.msh *.fasta # We sketch all the complete plasmid sequences 
mash dist reference.msh reference.msh -t -p 10 > complete_mash_distances.txt 
```

# Network of Mash distances

``` r
## Integration of the 26 complete plasmid sequences with 60 retrieved plsdb sequences

pl_distances <- read.table(file = '../mash/complete_genomes/complete_mash_distances.txt')
pl_bins <- pl_distances[,1]
pl_mash_df <- pl_distances[,c(2:ncol(pl_distances))]

# Cleaning the names  
pl_bins <- gsub('.fasta','',pl_bins)
pl_bins <- gsub('.fa','',pl_bins)


# Creating a dataframe with the pairwise distances 

colnames(pl_mash_df) <- pl_bins
rownames(pl_mash_df) <- pl_bins

pl_dist_mash <- as.dist(pl_mash_df)

plot_mash <- as.data.frame(pl_mash_df) %>%
  gather(key = bin, value = mash_distance)

plot_mash$bin_versus <- pl_bins

plot_mash <- subset(plot_mash, plot_mash$bin != plot_mash$bin_versus)

# Generating a dataframe that we can later use for igraph

weight_graph <- data.frame(From_to = plot_mash$bin,
                           To_from = plot_mash$bin_versus,
                           weight = as.numeric(as.character(plot_mash$mash_distance)))

threshold_distribution <- ggplot(weight_graph, aes(x=weight)) + 
    geom_histogram(aes(y=..density..),      # Histogram with density instead of count on y-axis
                   binwidth=.005,
                   colour="black", fill="white") +
    geom_vline(xintercept = 0.025, linetype = 'dashed') +
    geom_density(alpha=.2, fill="red") + 
    labs(x = 'Mash Distance (k = 21, s = 1,000)')

threshold_distribution # This is the distribution of Mash distances between all complete plasmid sequences 
```

![](report_files/figure-gfm/unnamed-chunk-32-1.png)<!-- -->

Drawing the network of complete plasmid sequences after removing the
weight

``` r
# Filtering out connections with a distance inferior to 0.025
filtered_graph <- subset(weight_graph, weight_graph$weight < 0.025)

filtered_graph$weight <- 1-filtered_graph$weight

# We remove self-connections between complete plasmid sequences
filtered_graph <- subset(filtered_graph, filtered_graph$From_to != filtered_graph$To_from)

# We create a column with the width of edges (network), we multiply it by 2 so it is more visible in the plot
filtered_graph$width <- filtered_graph$weight*2

# We create the network with igraph 
graph_pairs <- graph_from_data_frame(filtered_graph, directed = FALSE)

# We check that the network is weighted based on the distances 
is.weighted(graph_pairs)
```

    ## [1] TRUE

``` r
# Size of the nodes 
V(graph_pairs)$size <- 10

coords <- layout.fruchterman.reingold(graph_pairs) 

# Partitioning the subgraph into different clusters using the Louvain method 

graph_louvain <- cluster_louvain(graph_pairs)
modularity(graph_louvain)
```

    ## [1] 0.3101171

``` r
# We can retrieve the clusters found by the partitioning algorithms 

info_louvain <- data.frame(Cluster = graph_louvain$membership,
                            Strain = graph_louvain$names)

info_compl_louvain <- info_louvain


typing_compl_plasmids <- info_compl_louvain

typing_compl_plasmids <- info_compl_louvain
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 1] <- 1
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 5] <- 5
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 6] <- 6
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 2] <- 2
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 4] <- 4
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 3] <- 3
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 7] <- 7
typing_compl_plasmids$Type[typing_compl_plasmids$Cluster == 8] <- 7


# Based on the coverage values that we will show later, we decided to split the Type 10 

typing_compl_plasmids$Type[which(typing_compl_plasmids$Strain %in% c('E6055_4','E8040_4','NZ_CP046078.1','E8202_3'))] <- 8

typing_compl_plasmids$Type[which(typing_compl_plasmids$Strain %in% c('E4239_3','E4227_3','MG674582.1','NC_010980.1','NC_008821.1','NC_008768.1','NC_011140.1'))] <- 9


correct_palette <- c(pal[1],pal[2],pal[4],pal[3],pal[5],pal[6],pal[10],pal[7],pal[9]) # We use the same colours that we used to define the plasmid bin groups 

V(graph_pairs)$color <- correct_palette[typing_compl_plasmids$Type]


# To have always the same orientation in the plot graph 

coords <- layout.fruchterman.reingold(graph_pairs) 

#write.csv(x = coords, file = '/home/sergi/Data/testing_gplas/second_rebuttal/compl_coordinates.csv', row.names = FALSE)

coords <- read.csv(file = '../mash/complete_genomes/compl_coordinates.csv')

coords$X <- NULL

coords <- as.matrix(coords)

V(graph_pairs)$label.color <- 'black'

plot(graph_pairs, layout = coords, margin = 0, vertex.label.cex=0.5) # This is the network of plasmid types 

text_legend <- c('Plasmid Type A','Plasmid Type B','Plasmid Type C','Plasmid Type D','Plasmid Type E','Plasmid Type F','Plasmid Type G','Plasmid Type H','Plasmid Type I')

colours_legend <- c(pal[1],pal[5],pal[6],pal[7],pal[9],pal[2],pal[4],pal[3],pal[10])

legend("topleft", legend=text_legend, col = colours_legend , bty = "o", pch=20 , pt.cex = 3, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.75))
```

![](report_files/figure-gfm/unnamed-chunk-33-1.png)<!-- -->

``` r
# Retrieving the complete plasmid sequences that corresponded to singletons since there were not part of the network 

all_pl <- unique(as.character(weight_graph$From_to))

singletons <- setdiff(all_pl, info_louvain$Strain)

df_singletons <- data.frame(Cluster = 12, Strain = singletons, Type = 12)

# These singletons were also part of the ANIm alignment coverage

# Pairwise alignment coverage 
coverage_pl <- read.table(file = '../pyani/ANIm_alignment_coverage.tab')

coverage_pl[coverage_pl > 1.0 ]<-1.0

coverage_matrix <- as.matrix(coverage_pl)

my_palette <- colorRampPalette(c("blue", "white", "red"))(n = 20)

hclustfunc <- function(x) hclust(x, method="ward.D2")
distfunc <- function(x) dist(x, method="euclidean")

# perform clustering on rows and columns
cl.row <- hclustfunc(distfunc(coverage_matrix))
cl.col <- hclustfunc(distfunc(t(coverage_matrix)))

cl.row$labels
```

    ##  [1] "E7067_5"       "E7025_5"       "NZ_LR135256.1" "E4239_3"      
    ##  [5] "NZ_CP046078.1" "E4227_3"       "NZ_CP041269.1" "E7160_5"      
    ##  [9] "NZ_CP027505.1" "NZ_CP027500.1" "KX810025.1"    "E8040_4"      
    ## [13] "NZ_AP022343.1" "NC_014726.1"   "NZ_CP018825.1" "NZ_CP033209.1"
    ## [17] "AP022823.1"    "NC_019213.1"   "NZ_CP016166.1" "E7471_5"      
    ## [21] "NC_013317.1"   "E7070_9"       "E7207_6"       "NZ_CP011284.1"
    ## [25] "MG674582.1"    "NZ_CP012594.1" "KX574671.1"    "E7240_4"      
    ## [29] "NC_021170.1"   "KY595966.1"    "KX976485.1"    "NC_014475.1"  
    ## [33] "NZ_CP013996.1" "NZ_CP041279.1" "NC_005054.1"   "NZ_CP022486.1"
    ## [37] "KY595962.1"    "NZ_CP027516.1" "E0656_2"       "NZ_CP035644.1"
    ## [41] "NZ_CP023796.1" "NC_008768.1"   "E8414_4"       "NC_014959.1"  
    ## [45] "NZ_CP014531.1" "KX853854.1"    "E6055_4"       "NZ_CP020486.1"
    ## [49] "NZ_CP018831.1" "E7040_5"       "NC_016967.1"   "KY554216.1"   
    ## [53] "E6988_5"       "E6975_4"       "KX810026.1"    "NZ_CP012431.1"
    ## [57] "E7313_3"       "NC_008821.1"   "E8423_3"       "NZ_CP040238.1"
    ## [61] "NZ_CP035650.1" "NC_011140.1"   "E8172_3"       "NZ_CP027519.1"
    ## [65] "E0595_2"       "NC_010980.1"   "E7246_6"       "E8202_3"      
    ## [69] "NZ_CP036247.1" "NZ_CP044276.1" "NZ_CP017795.1" "NZ_CP027510.1"
    ## [73] "E8014_3"       "NZ_CP012464.1" "NZ_CP019995.1" "E0139_2"      
    ## [77] "NZ_CP019210.1" "NZ_CP040708.1" "NZ_CP025755.1" "NZ_CP018130.1"
    ## [81] "NZ_LT603681.1" "E7114_4"       "NZ_CP014452.1" "E6020_3"      
    ## [85] "NZ_CP019973.1" "NC_019284.1"

``` r
# Singletons not present in the network 

setdiff(cl.row$labels,V(graph_pairs)$name)
```

    ##  [1] "NC_014726.1"   "AP022823.1"    "NC_013317.1"   "NZ_CP012594.1"
    ##  [5] "NC_005054.1"   "NZ_CP022486.1" "NZ_CP014531.1" "NZ_CP040238.1"
    ##  [9] "E8172_3"       "NZ_CP036247.1" "NZ_CP018130.1" "NZ_CP019973.1"

``` r
row_df <- data.frame(Strain = cl.row$labels)

total_info <- rbind(typing_compl_plasmids, df_singletons)

info_row <- merge(row_df, typing_compl_plasmids, by = 'Strain')

info_row <- info_row[match(cl.row$labels, typing_compl_plasmids$Strain),]

correct_palette <- c(pal[1],pal[2],pal[4],pal[3],pal[5],pal[6],pal[10],pal[7],pal[9])

heatmap.2(as.matrix(coverage_pl), hclustfun=hclustfunc, distfun=distfunc, col = my_palette, tracecol = NA, RowSideColors = correct_palette[info_row$Type], keysize = 1.0, margins = c(6, 6), densadj = 1)
```

![](report_files/figure-gfm/unnamed-chunk-34-1.png)<!-- -->

``` r
# Statistics behind 

# Putting the data into a dataframe
coverage_pl_df <- as.data.frame(coverage_pl) %>%
  gather(key = bin, value = ANI_coverage)
coverage_pl_df$versus_bin <- colnames(coverage_pl)

# Avoiding self-comparisons between the same plasmid 
coverage_pl_df <- subset(coverage_pl_df, coverage_pl_df$bin != coverage_pl_df$versus_bin)

# Cleaning the names of the files 
coverage_pl_df$Strain <- coverage_pl_df$bin
coverage_pl_df$Strain <- gsub(pattern = 'pl', replacement = '', x = coverage_pl_df$Strain)

coverage_pl_df$Strain_versus <- coverage_pl_df$versus_bin
coverage_pl_df$Strain_versus <- gsub(pattern = 'pl', replacement = '', x = coverage_pl_df$Strain_versus)

# Incorporating information regarding the plasmid configurations
coverage_configs <- merge(coverage_pl_df, typing_compl_plasmids, by = 'Strain')

colnames(coverage_configs)[1] <- 'Isolate'
colnames(coverage_configs)[5] <- 'Strain'

coverage_configs <- merge(coverage_configs, typing_compl_plasmids, by = 'Strain')

# Dividing the pairwise comparisons based on if they connect plasmids from the same configuration (intra) or different configurations (inter)
coverage_configs$event <- ifelse(coverage_configs$Type.x == coverage_configs$Type.y, 'Intra', 'Inter')

# Stats behind
cov_report <- coverage_configs %>%
  group_by(Type.x, event) %>%
  summarize(avg_cov = mean(ANI_coverage))

cov_report$round_cov <- round(cov_report$avg_cov,2)

cov_report %>%
  group_by(event) %>%
  summarize(mean(round_cov))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["event"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean(round_cov)"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Inter","2":"0.3155556"},{"1":"Intra","2":"0.7911111"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# We exactly do the same analysis but looking at the identity 

identity_pl <- read.table(file = '../pyani/ANIm_percentage_identity.tab')


identity_pl_df <- as.data.frame(identity_pl) %>%
  gather(key = bin, value = ANI_identity)
identity_pl_df$versus_bin <- colnames(identity_pl)

identity_pl_df$ANI_identity[which(identity_pl_df$ANI_identity > 1.0)] <- 1.0

identity_pl_df <- subset(identity_pl_df, identity_pl_df$bin != identity_pl_df$versus_bin)

identity_pl_df$Strain <- identity_pl_df$bin
identity_pl_df$Strain <- gsub(pattern = 'pl', replacement = '', x = identity_pl_df$Strain)

identity_pl_df$Strain_versus <- identity_pl_df$versus_bin
identity_pl_df$Strain_versus <- gsub(pattern = 'pl', replacement = '', x = identity_pl_df$Strain_versus)

identity_configs <- merge(identity_pl_df, typing_compl_plasmids, by = 'Strain')

colnames(identity_configs)[1] <- 'Isolate'
colnames(identity_configs)[5] <- 'Strain'

identity_configs <- merge(identity_configs, typing_compl_plasmids, by = 'Strain')


identity_configs$event <- ifelse(identity_configs$Type.x == identity_configs$Type.y, 'Intra', 'Inter')

id_report <- identity_configs %>%
  group_by(Type.x, event) %>%
  summarize(avg_cov = mean(ANI_identity))

id_report$round_cov <- round(id_report$avg_cov,2)

id_report %>%
  group_by(event) %>%
  summarize(mean(round_cov))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["event"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean(round_cov)"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Inter","2":"0.9888889"},{"1":"Intra","2":"0.9966667"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

# Network of plasmid bins and complete plasmid sequences

## Pairwise distances between complete plasmid sequences

``` bash

mash sketch -k 21 -s 1000 -p 10 -o  reference.msh *.fasta # We sketch all the sequences carrying the Tn1546 sequence (either predicted bins or complete sequences)
mash dist reference.msh reference.msh -t -p 10 > mash_distances.txt 
```

We can combine the prediction of the plasmid bins together with the
complete plasmid sequences for which we have defined plasmid types. If
the prediction of these plasmid bins would have been completely
different from the set of complete plasmid sequences, we could argue
that the prediction is wrong since its k-mer composition does not
resemble any ground truth sequenced before with the help of long-read
sequencing
data

``` r
distances <- read.table(file = '../mash/both_sequence_types/mash_distances.txt')
bins <- distances[,1]
mash_df <- distances[,c(2:ncol(distances))]
  
bins <- gsub('.fasta.gz','',bins)
bins <- gsub('.fasta','',bins)
bins <- gsub('.fa','',bins)
bins <- gsub('.fa.gz','',bins)

colnames(mash_df) <- bins
rownames(mash_df) <- bins

dist_mash <- as.dist(mash_df)

plot_mash <- as.data.frame(mash_df) %>%
  gather(key = bin, value = mash_distance)

plot_mash$bin_versus <- bins

#plot_mash <- plot_mash[grep(pattern = '_bin|pl|NZ|NC|AP|K|MG', x = plot_mash$bin),]
#plot_mash <- plot_mash[grep(pattern = '_bin|pl', x = plot_mash$bin_versus),]

plot_mash <- plot_mash[- grep(pattern = '_Unbinned', x = plot_mash$bin),]
plot_mash <- plot_mash[- grep(pattern = '_Unbinned', x = plot_mash$bin_versus),]

# We load again abricate results 
abricate_resfinder <- read.csv(file = '../abricate/abricate_results.csv')

van_results <- subset(abricate_resfinder, abricate_resfinder$GENE == 'VanA_2')
van_bins <- van_results$X.FILE
van_bins <- gsub(pattern = '.fasta', replacement = '', x = van_bins)

keep_seq <- as.character(total_info$Strain)

van_bins <- c(van_bins, keep_seq)

plot_mash <- subset(plot_mash, plot_mash$bin %in% van_bins)
plot_mash <- subset(plot_mash, plot_mash$bin_versus %in% van_bins)

plot_mash <- plot_mash[!duplicated(plot_mash),]

plot_mash <- subset(plot_mash, plot_mash$bin != plot_mash$bin_versus)

strains <- unique(str_split_fixed(pattern = '_', string = bins, n = 3)[,1])

# Graph viz 

weight_graph <- data.frame(From_to = plot_mash$bin,
                           To_from = plot_mash$bin_versus,
                           weight = as.numeric(as.character(plot_mash$mash_distance)))
weight_graph$Strain <- str_split_fixed(string = weight_graph$From_to, pattern = '_', n = 2)[,1]
weight_graph$Pair_Strain <- str_split_fixed(string = weight_graph$To_from, pattern = '_', n = 2)[,1]


# Threshold to draw an edge (0.025)

filtered_graph <- weight_graph

# Filtering out connections with a distance inferior to 0.025
filtered_graph <- subset(weight_graph, weight_graph$weight < 0.025)

filtered_graph$weight <- 1-filtered_graph$weight

filtered_graph$From_to <- as.character(filtered_graph$From_to)
filtered_graph$To_from <- as.character(filtered_graph$To_from)

filtered_graph <- subset(filtered_graph, filtered_graph$From_to != filtered_graph$To_from)

filtered_graph$width <- filtered_graph$weight

graph_pairs <- graph_from_data_frame(filtered_graph, directed = FALSE)

is.weighted(graph_pairs)
```

    ## [1] TRUE

``` r
vertices_graph <- V(graph_pairs)

binnames <- grep('bin', x = V(graph_pairs)$name)

#V(graph_pairs)$name[binnames] <- ''
V(graph_pairs)$size <- 2.0
V(graph_pairs)$shape <- 'square'
V(graph_pairs)$shape[binnames] <- 'circle'

plot(graph_pairs)
```

![](report_files/figure-gfm/unnamed-chunk-38-1.png)<!-- -->

``` r
# Coloured by assignment 

names_nodes <- V(graph_pairs)$name
names_nodes
```

    ##   [1] "E0139_2"       "E0595_2"       "E0656_2"       "E4227_3"      
    ##   [5] "E4239_3"       "E6020_3"       "E6055_4"       "E6975_4"      
    ##   [9] "E6988_5"       "E7025_5"       "E7040_5"       "E7067_5"      
    ##  [13] "E7070_9"       "E7114_4"       "E7160_5"       "E7207_6"      
    ##  [17] "E7240_4"       "E7246_6"       "E7308_bin_3"   "E7309_bin_1"  
    ##  [21] "E7310_bin_1"   "E7311_bin_1"   "E7312_bin_2"   "E7313_3"      
    ##  [25] "E7313_bin_5"   "E7321_bin_1"   "E7323_bin_5"   "E7324_bin_2"  
    ##  [29] "E7325_bin_1"   "E7326_bin_3"   "E7327_bin_5"   "E7330_bin_1"  
    ##  [33] "E7331_bin_1"   "E7332_bin_4"   "E7333_bin_2"   "E7336_bin_1"  
    ##  [37] "E7342_bin_3"   "E7366_bin_3"   "E7368_bin_2"   "E7369_bin_1"  
    ##  [41] "E7377_bin_2"   "E7392_bin_3"   "E7404_bin_1"   "E7405_bin_3"  
    ##  [45] "E7413_bin_3"   "E7414_bin_4"   "E7415_bin_3"   "E7416_bin_5"  
    ##  [49] "E7417_bin_2"   "E7418_bin_1"   "E7419_bin_3"   "E7432_bin_4"  
    ##  [53] "E7470_bin_3"   "E7471_5"       "E7474_bin_6"   "E7475_bin_3"  
    ##  [57] "E7476_bin_5"   "E7477_bin_2"   "E7478_bin_1"   "E7479_bin_4"  
    ##  [61] "E7480_bin_3"   "E7481_bin_3"   "E7482_bin_4"   "E7483_bin_1"  
    ##  [65] "E7484_bin_3"   "E7485_bin_3"   "E7495_bin_2"   "E7496_bin_2"  
    ##  [69] "E7497_bin_5"   "E7498_bin_4"   "E7499_bin_3"   "E7500_bin_5"  
    ##  [73] "E7501_bin_1"   "E7502_bin_4"   "E7503_bin_1"   "E7504_bin_1"  
    ##  [77] "E7513_bin_1"   "E7516_bin_4"   "E7519_bin_3"   "E7520_bin_5"  
    ##  [81] "E7521_bin_1"   "E7522_bin_2"   "E7523_bin_2"   "E7524_bin_5"  
    ##  [85] "E7525_bin_4"   "E7526_bin_1"   "E7527_bin_3"   "E7528_bin_4"  
    ##  [89] "E7529_bin_4"   "E7530_bin_3"   "E7531_bin_1"   "E7535_bin_4"  
    ##  [93] "E7536_bin_4"   "E7537_bin_1"   "E7538_bin_1"   "E7539_bin_1"  
    ##  [97] "E7543_bin_1"   "E7544_bin_4"   "E7545_bin_1"   "E7546_bin_1"  
    ## [101] "E7547_bin_3"   "E7548_bin_4"   "E7549_bin_4"   "E7550_bin_3"  
    ## [105] "E7551_bin_1"   "E7554_bin_2"   "E7555_bin_1"   "E7556_bin_1"  
    ## [109] "E7573_bin_1"   "E7575_bin_1"   "E7576_bin_1"   "E7577_bin_6"  
    ## [113] "E7578_bin_4"   "E7579_bin_2"   "E7580_bin_1"   "E7581_bin_5"  
    ## [117] "E7585_bin_3"   "E7586_bin_3"   "E7587_bin_4"   "E7588_bin_4"  
    ## [121] "E7595_bin_4"   "E7596_bin_2"   "E7603_bin_1"   "E7608_bin_3"  
    ## [125] "E7609_bin_1"   "E7641_bin_7"   "E7672_bin_1"   "E7673_bin_1"  
    ## [129] "E7678_bin_1"   "E7684_bin_1"   "E7685_bin_1"   "E7688_bin_1"  
    ## [133] "E7696_bin_3"   "E7697_bin_5"   "E7698_bin_5"   "E7699_bin_3"  
    ## [137] "E7700_bin_1"   "E7703_bin_1"   "E7836_bin_3"   "E7837_bin_4"  
    ## [141] "E7850_bin_1"   "E7858_bin_3"   "E7876_bin_4"   "E7877_bin_3"  
    ## [145] "E7882_bin_1"   "E7884_bin_2"   "E7885_bin_4"   "E7886_bin_4"  
    ## [149] "E7932_bin_3"   "E7941_bin_1"   "E7963_bin_4"   "E7964_bin_3"  
    ## [153] "E7965_bin_5"   "E7966_bin_5"   "E7967_bin_5"   "E7968_bin_4"  
    ## [157] "E7969_bin_3"   "E7986_bin_4"   "E7987_bin_5"   "E7989_bin_4"  
    ## [161] "E7990_bin_3"   "E7991_bin_3"   "E7992_bin_5"   "E7993_bin_1"  
    ## [165] "E7994_bin_1"   "E7995_bin_1"   "E7996_bin_1"   "E7999_bin_3"  
    ## [169] "E8001_bin_3"   "E8002_bin_1"   "E8003_bin_4"   "E8004_bin_4"  
    ## [173] "E8008_bin_3"   "E8009_bin_1"   "E8010_bin_1"   "E8011_bin_4"  
    ## [177] "E8012_bin_1"   "E8013_bin_1"   "E8014_3"       "E8014_bin_1"  
    ## [181] "E8015_bin_3"   "E8016_bin_4"   "E8017_bin_4"   "E8018_bin_3"  
    ## [185] "E8020_bin_1"   "E8026_bin_1"   "E8028_bin_1"   "E8033_bin_4"  
    ## [189] "E8034_bin_4"   "E8035_bin_3"   "E8039_bin_2"   "E8040_4"      
    ## [193] "E8041_bin_3"   "E8042_bin_5"   "E8044_bin_4"   "E8045_bin_4"  
    ## [197] "E8046_bin_2"   "E8048_bin_1"   "E8049_bin_2"   "E8057_bin_3"  
    ## [201] "E8128_bin_1"   "E8129_bin_4"   "E8150_bin_3"   "E8155_bin_3"  
    ## [205] "E8156_bin_4"   "E8157_bin_3"   "E8158_bin_3"   "E8164_bin_4"  
    ## [209] "E8167_bin_4"   "E8168_bin_3"   "E8170_bin_4"   "E8172_3"      
    ## [213] "E8172_bin_4"   "E8173_bin_4"   "E8174_bin_4"   "E8175_bin_3"  
    ## [217] "E8176_bin_1"   "E8177_bin_1"   "E8180_bin_1"   "E8182_bin_4"  
    ## [221] "E8184_bin_5"   "E8188_bin_1"   "E8189_bin_1"   "E8191_bin_3"  
    ## [225] "E8192_bin_3"   "E8193_bin_3"   "E8201_bin_1"   "E8202_3"      
    ## [229] "E8202_bin_1"   "E8205_bin_1"   "E8209_bin_1"   "E8210_bin_1"  
    ## [233] "E8223_bin_2"   "E8224_bin_1"   "E8233_bin_1"   "E8237_bin_6"  
    ## [237] "E8303_bin_4"   "E8321_bin_2"   "E8323_bin_1"   "E8339_bin_5"  
    ## [241] "E8340_bin_4"   "E8341_bin_4"   "E8343_bin_3"   "E8344_bin_3"  
    ## [245] "E8345_bin_1"   "E8346_bin_4"   "E8347_bin_2"   "E8348_bin_2"  
    ## [249] "E8349_bin_2"   "E8350_bin_2"   "E8351_bin_2"   "E8386_bin_4"  
    ## [253] "E8387_bin_5"   "E8388_bin_1"   "E8390_bin_3"   "E8391_bin_5"  
    ## [257] "E8392_bin_1"   "E8393_bin_4"   "E8394_bin_4"   "E8395_bin_5"  
    ## [261] "E8398_bin_4"   "E8399_bin_5"   "E8402_bin_4"   "E8403_bin_1"  
    ## [265] "E8410_bin_2"   "E8414_4"       "E8415_bin_2"   "E8416_bin_3"  
    ## [269] "E8421_bin_3"   "E8423_3"       "E8423_bin_4"   "E8427_bin_2"  
    ## [273] "E8431_bin_4"   "E8437_bin_1"   "E8438_bin_1"   "E8439_bin_2"  
    ## [277] "E8441_bin_5"   "E8442_bin_2"   "E8444_bin_3"   "E8447_bin_1"  
    ## [281] "E8448_bin_1"   "E8449_bin_5"   "E8450_bin_1"   "E8454_bin_4"  
    ## [285] "E8455_bin_5"   "E8459_bin_1"   "E9000_bin_1"   "E9002_bin_1"  
    ## [289] "E9005_bin_1"   "E9006_bin_5"   "E9007_bin_3"   "E9009_bin_4"  
    ## [293] "E9010_bin_1"   "E9015_bin_1"   "E9016_bin_1"   "E9017_bin_1"  
    ## [297] "KX574671.1"    "KX810025.1"    "KX810026.1"    "KX853854.1"   
    ## [301] "KX976485.1"    "KY554216.1"    "KY595962.1"    "KY595966.1"   
    ## [305] "MG674582.1"    "NC_008768.1"   "NC_008821.1"   "NC_010980.1"  
    ## [309] "NC_011140.1"   "NC_013317.1"   "NC_014475.1"   "NC_014959.1"  
    ## [313] "NC_016967.1"   "NC_019213.1"   "NC_019284.1"   "NC_021170.1"  
    ## [317] "NZ_AP022343.1" "NZ_CP011284.1" "NZ_CP012431.1" "NZ_CP012464.1"
    ## [321] "NZ_CP013996.1" "NZ_CP014452.1" "NZ_CP016166.1" "NZ_CP017795.1"
    ## [325] "NZ_CP018130.1" "NZ_CP018825.1" "NZ_CP018831.1" "NZ_CP019210.1"
    ## [329] "NZ_CP019995.1" "NZ_CP020486.1" "NZ_CP022486.1" "NZ_CP023796.1"
    ## [333] "NZ_CP025755.1" "NZ_CP027500.1" "NZ_CP027505.1" "NZ_CP027510.1"
    ## [337] "NZ_CP027516.1" "NZ_CP027519.1" "NZ_CP033209.1" "NZ_CP035644.1"
    ## [341] "NZ_CP035650.1" "NZ_CP040708.1" "NZ_CP041269.1" "NZ_CP041279.1"
    ## [345] "NZ_CP044276.1" "NZ_CP046078.1" "NZ_LR135256.1" "NZ_LT603681.1"

``` r
df_nodes <- data.frame(Strain = names_nodes)

df_nodes$Strain <- gsub(pattern = '_bin_[0-9]',replacement = '', x = df_nodes$Strain)

df_names <- df_nodes

df_nodes <- merge(df_nodes, combination_vana_isolates, by = 'Strain')


df_nodes$colors <- ifelse(df_nodes$vanA_cluster == 1, 'lightblue',
ifelse(df_nodes$vanA_cluster == 2, 'purple',
ifelse(df_nodes$vanA_cluster == 3, 'orange',
ifelse(df_nodes$vanA_cluster == 4, 'forestgreen',
ifelse(df_nodes$vanA_cluster == 5, 'brown', 
ifelse(df_nodes$vanA_cluster == 6, 'red',
ifelse(df_nodes$vanA_cluster == 7, 'black',
ifelse(df_nodes$vanA_cluster == 8, 'darkblue','pink'))))))))

df_nodes$colors[which(is.na(df_nodes$vanA_cluster) == TRUE)] <- 'gray'

df_colors <- select(df_nodes, c('Strain','colors'))

setdiff(df_names$Strain, df_nodes$Strain)
```

    ##  [1] "E0139_2"       "E0595_2"       "E0656_2"       "E4227_3"      
    ##  [5] "E4239_3"       "E6020_3"       "E6055_4"       "E6975_4"      
    ##  [9] "E6988_5"       "E7025_5"       "E7040_5"       "E7067_5"      
    ## [13] "E7070_9"       "E7114_4"       "E7160_5"       "E7207_6"      
    ## [17] "E7240_4"       "E7246_6"       "E7313_3"       "E7471_5"      
    ## [21] "E8014_3"       "E8040_4"       "E8172_3"       "E8202_3"      
    ## [25] "E8414_4"       "E8423_3"       "KX574671.1"    "KX810025.1"   
    ## [29] "KX810026.1"    "KX853854.1"    "KX976485.1"    "KY554216.1"   
    ## [33] "KY595962.1"    "KY595966.1"    "MG674582.1"    "NC_008768.1"  
    ## [37] "NC_008821.1"   "NC_010980.1"   "NC_011140.1"   "NC_013317.1"  
    ## [41] "NC_014475.1"   "NC_014959.1"   "NC_016967.1"   "NC_019213.1"  
    ## [45] "NC_019284.1"   "NC_021170.1"   "NZ_AP022343.1" "NZ_CP011284.1"
    ## [49] "NZ_CP012431.1" "NZ_CP012464.1" "NZ_CP013996.1" "NZ_CP014452.1"
    ## [53] "NZ_CP016166.1" "NZ_CP017795.1" "NZ_CP018130.1" "NZ_CP018825.1"
    ## [57] "NZ_CP018831.1" "NZ_CP019210.1" "NZ_CP019995.1" "NZ_CP020486.1"
    ## [61] "NZ_CP022486.1" "NZ_CP023796.1" "NZ_CP025755.1" "NZ_CP027500.1"
    ## [65] "NZ_CP027505.1" "NZ_CP027510.1" "NZ_CP027516.1" "NZ_CP027519.1"
    ## [69] "NZ_CP033209.1" "NZ_CP035644.1" "NZ_CP035650.1" "NZ_CP040708.1"
    ## [73] "NZ_CP041269.1" "NZ_CP041279.1" "NZ_CP044276.1" "NZ_CP046078.1"
    ## [77] "NZ_LR135256.1" "NZ_LT603681.1"

``` r
df_compl_colors <- data.frame(Strain = setdiff(df_names$Strain, df_nodes$Strain),
                              colors = 'white')

df_colors <- rbind(df_colors, df_compl_colors)

order_strains <- gsub(pattern = '_bin_[0-9]',replacement = '', x = V(graph_pairs)$name)

df_colors <- df_colors[match(order_strains,df_colors$Strain),]


df_colors$colors[df_colors$colors == 'forestgreen'] <- pal[5]
df_colors$colors[df_colors$colors == 'orange'] <- pal[6]
df_colors$colors[df_colors$colors == 'red'] <- pal[7]
df_colors$colors[df_colors$colors == 'black'] <- pal[1]
df_colors$colors[df_colors$colors == 'brown'] <- pal[10]
df_colors$colors[df_colors$colors == 'purple'] <- pal[11]
df_colors$colors[df_colors$colors == 'darkblue'] <- pal[9]
df_colors$colors[df_colors$colors == 'lightblue'] <- 'orange'

binnames <- grep('bin', x = V(graph_pairs)$name)

V(graph_pairs)$name[binnames] <- ''
V(graph_pairs)$size <- 5.0
V(graph_pairs)$size[binnames] <- 2.0
V(graph_pairs)$shape <- 'square'
V(graph_pairs)$shape[binnames] <- 'circle'
V(graph_pairs)$color <- df_colors$colors

typing_compl_plasmids$Type[typing_compl_plasmids$Type == 1] <- "A"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 2] <- "F"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 3] <- "G"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 4] <- "H"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 5] <- "B"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 6] <- "C"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 7] <- "I"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 8] <- "D"
typing_compl_plasmids$Type[typing_compl_plasmids$Type == 9] <- "E"

typing_compl_plasmids$Type[which(typing_compl_plasmids$Strain %in% c('E6055_4','E8040_4','NZ_CP046078.1','E8202_3'))] <- "D"

typing_compl_plasmids$Type[which(typing_compl_plasmids$Strain %in% c('E4239_3','E4227_3','MG674582.1','NC_010980.1','NC_008821.1','NC_008768.1','NC_011140.1'))] <- "E"


compl_pl <- which(V(graph_pairs)$name %in% typing_compl_plasmids$Strain) 

singletons <- grep(pattern = 'N', x = V(graph_pairs)$name)
extra_singleton <- grep(pattern = 'E8172_3', x = V(graph_pairs)$name)
singletons <- c(singletons,extra_singleton)

V(graph_pairs)$name[singletons] <- '*'

V(graph_pairs)$name[compl_pl] <- typing_compl_plasmids$Type


coords <- layout.fruchterman.reingold(graph_pairs) 

# To have always the same orientation in the plot graph 

#write.csv(x = coords, file = '/home/sergi/Data/testing_gplas/final_rebuttal_coordinates.csv', row.names = FALSE)

coords <- read.csv(file = '../mash/both_sequence_types/network_coordinates.csv')

coords$X <- NULL

coords <- as.matrix(coords)

plot(graph_pairs, layout = coords)


legend("topleft", legend=c('Complete Plasmid','Predicted Bin'), col = 'gray' , bty = "o", pch= c(0,1) , pt.cex = 2, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.84))


# We create a legend with the assignment of colours based on SC
text_legend <- sort(unique(combination_vana_isolates$vanA_cluster))
text_legend <- c(text_legend,'(Unassigned)')

text_legend <- paste(text_legend, c('- Plasmid Type J','- Plasmid Type C','- Plasmid Type C','- Plasmid Type B','- Plasmid Type I','- Plasmid Type D','- Plasmid Type A','- Plasmid Type E',''))

colours_legend <- c('orange',pal[11],pal[6],pal[5],pal[10],pal[7],pal[1],pal[9],'gray')

legend("topleft", legend=paste('Bin Group',text_legend), col = colours_legend , bty = "o", pch=20 , pt.cex = 2, cex = 1.0, horiz = FALSE, inset = c(0.0, 0.9))
```

![](report_files/figure-gfm/unnamed-chunk-38-2.png)<!-- -->

# Modes of vanA dissemination

``` r
# Hospital cases, 309 isolates

hosp_cases <- vana_isolates

unique(hosp_cases$Hospital) # in total 32 Dutch hospitals involved in this collection 
```

    ##  [1] Antonius location Oudenrijn    Maasstad Rotterdam            
    ##  [3] Viecuri MC Venlo               OLVG, locatie oost            
    ##  [5] MC Zuiderzee                   Isala klinieken Zwolle        
    ##  [7] Antonius location Nieuwegein   AZM                           
    ##  [9] Flevoziekenhuis                UMC Utrecht                   
    ## [11] UMCG                           Orbis Medisch Centrum         
    ## [13] SLAZ                           Laurentius Ziekenhuis Roermond
    ## [15] Rijnstate Arnhem               Admiraal de Ruyter ziekenhuis 
    ## [17] Amphia ziekenhuis Breda        Gelderse Vallei               
    ## [19] Atrium                         Meander Medisch Centrum       
    ## [21] Westfries Gasthuis             AMC                           
    ## [23] Erasmus MC                     LUMC                          
    ## [25] MST                            MC Alkmaar                    
    ## [27] Albert Schweitzer              Zuwe Hofpoort                 
    ## [29] Westeinde                      Spaarne ziekenhuis            
    ## [31] Diakonessenhuis Utrecht        Leyenburg                     
    ## 64 Levels:  ?? Admiraal de Ruyter ziekenhuis Albert Schweitzer ... Zuwe Hofpoort

``` r
# We classify the hospitals depending on the region in which they are located 

hosp_cases$Region <- ifelse(hosp_cases$Hospital %in% c('Antonius location Oudenrijn', 'Antonius location Nieuwegein','UMC Utrecht','Diakonessenhuis Utrecht','Meander Medisch Centrum','Zuwe Hofpoort'), 'Utrecht',
ifelse(hosp_cases$Hospital %in% c('Maasstad Rotterdam','Albert Schweitzer','Erasmus MC','LUMC','Westeinde','Leyenburg'), 'South-Holland',
ifelse(hosp_cases$Hospital %in% c('Viecuri MC Venlo','Orbis Medisch Centrum','Atrium','Laurentius Ziekenhuis Roermond','AZM'), 'Limburg',
ifelse(hosp_cases$Hospital %in% c('MC Zuiderzee','Flevoziekenhuis'), 'Flevoland',
ifelse(hosp_cases$Hospital %in% c('Isala klinieken Zwolle','MST'),'Overijssel',
ifelse(hosp_cases$Hospital %in% c('UMCG'),'Groningen',
ifelse(hosp_cases$Hospital %in% c('SLAZ','AMC','Westfries Gasthuis','OLVG, locatie oost','MC Alkmaar','Spaarne ziekenhuis'),'North-Holland',
ifelse(hosp_cases$Hospital %in% c('Rijnstate Arnhem','Gelderse Vallei'),'Gelderland',
ifelse(hosp_cases$Hospital %in% c('Admiraal de Ruyter ziekenhuis'),'Zeeland',
ifelse(hosp_cases$Hospital %in% c('Amphia ziekenhuis Breda'),'North-Brabant','unassigned'))))))))))

# Information from all the 225 isolates for which we have SC, plasmid type and Tn1546 assignments

# Important, only in this analysis 225 isolates are included since for them we have all complete assignments 

meta_info <- subset(annotation_tn_meta, select = c('label','Strain','Poppunk_cluster','vanA_cluster','SC','Tn_variant','Combi'))

# The plasmid bin groups 2-3 are considered as carrying the same k-mer composition. The prediction of plasmid bin 2 was erroneous and it was a limitation that we observed in the gplas prediction 

meta_info$vanA_cluster <- gsub(pattern = "2|3", replacement = "2-3", x = meta_info$vanA_cluster)

# We provide to the readers in Additional File S3 with all the information from the 225 isolates used in this analysis 

table_S3 <- merge(hosp_cases, meta_info, by = 'Strain')

table_S3 <- subset(table_S3, select = c('Strain','SC','vanA_cluster','Tn_variant','Combi','Isolation.date','Hospital','Region'))

table_S3$Plasmid_type <- ifelse(table_S3$vanA_cluster == '1', 'J',
ifelse(table_S3$vanA_cluster == '2-3','C',
ifelse(table_S3$vanA_cluster == '4', 'B',
ifelse(table_S3$vanA_cluster == '5', 'I',
ifelse(table_S3$vanA_cluster == '6', 'D',
ifelse(table_S3$vanA_cluster == '7', 'A',
ifelse(table_S3$vanA_cluster == '8', 'E','Unassigned')))))))

table_S3$vanA_cluster <- NULL

table_S3$Combi <- gsub(pattern = 'none_', replacement = '', x = table_S3$Combi)
table_S3$Combi <- gsub(pattern = '_none', replacement = '', x = table_S3$Combi)
table_S3$Combi <- gsub(pattern = '_', replacement = '|', x = table_S3$Combi)

table_S3$Hospital <- gsub(pattern = ',', replacement = ' ', x = table_S3$Hospital)

colnames(table_S3) <- c('Isolate','SC','Tn1546_variant','Tn1546_description(Deletions+SNPs)','Isolation_date','Hospital','Region','Plasmid_type')

table_S3 <- subset(table_S3, select = c('Isolate','SC','Plasmid_type','Tn1546_variant','Tn1546_description(Deletions+SNPs)','Isolation_date','Hospital','Region'))
#write.csv(x = table_S3, file = '/home/sergi/Data/dissemination_plasmids/figures/neat/journal/Genome_Medicine/Rebuttal/Second_Rebuttal/Additional_File_S3.csv', quote = FALSE, row.names = FALSE)

# The approach consisted in creating all possible pairwise comparison between the isolates and later grouped them based on isolates recovered within 12 month. Then, we considered isolates on a country-wide perspective, regional perspective or hospital perspective. 

comparison_strains <- NULL

for(strain in meta_info$Strain)
{
  info_strain <- subset(meta_info, meta_info$Strain  == strain)
  side_by_side <- cbind(info_strain, meta_info)
  comparison_strains <- rbind(comparison_strains, side_by_side)
  
}

colnames(comparison_strains) <- c('original_label', 'original_Strain','PopPUNK_cluster','vanAtype','SC','Tn_variant','Combi','pair_label','pair_Strain','pair_PopPUNK','pair_vanAtype','pair_SC','pair_Tn_variant','pair_Combi')

# We deleted self-comparisons                                  
comparison_strains <- subset(comparison_strains, comparison_strains$original_Strain != comparison_strains$pair_Strain)

# Here we define the most likely scenarios of dissemination based on if the pairs of isolates share: SC, plasmid type and Tn1546 variants 
comparison_strains$Evaluation <- ifelse(comparison_strains$SC == comparison_strains$pair_SC & comparison_strains$vanAtype == comparison_strains$pair_vanAtype & comparison_strains$Tn_variant == comparison_strains$pair_Tn_variant, 'Clonal (SC+Plasmid+Tn)',
ifelse(comparison_strains$SC != comparison_strains$pair_SC & comparison_strains$vanAtype == comparison_strains$pair_vanAtype & comparison_strains$Tn_variant == comparison_strains$pair_Tn_variant, 'HGT(Plasmid+Tn)',
ifelse(comparison_strains$SC != comparison_strains$pair_SC & comparison_strains$vanAtype == comparison_strains$pair_vanAtype & comparison_strains$Tn_variant != comparison_strains$pair_Tn_variant, 'Unrelated',
ifelse(comparison_strains$SC != comparison_strains$pair_SC & comparison_strains$vanAtype != comparison_strains$pair_vanAtype & comparison_strains$Tn_variant == comparison_strains$pair_Tn_variant, 'Mixed (Transposon)', 
ifelse(comparison_strains$SC == comparison_strains$pair_SC & comparison_strains$vanAtype != comparison_strains$pair_vanAtype & comparison_strains$Tn_variant == comparison_strains$pair_Tn_variant, 'Clonal (SC+Transposon)', 
ifelse(comparison_strains$SC == comparison_strains$pair_SC & comparison_strains$vanAtype == comparison_strains$pair_vanAtype & comparison_strains$Tn_variant != comparison_strains$pair_Tn_variant, 'Unrelated',  
ifelse(comparison_strains$SC != comparison_strains$pair_SC & comparison_strains$vanAtype != comparison_strains$pair_vanAtype & comparison_strains$Tn_variant != comparison_strains$pair_Tn_variant, 'Unrelated',
ifelse(comparison_strains$SC == comparison_strains$pair_SC & comparison_strains$vanAtype != comparison_strains$pair_vanAtype & comparison_strains$Tn_variant != comparison_strains$pair_Tn_variant, 'Unrelated',
'unassigned'))))))))

comparison_strains$Traditional_Evaluation <- ifelse(comparison_strains$SC == comparison_strains$pair_SC, 'Clonal','No-transfer')

colnames(hosp_cases)[1] <- 'original_Strain'
final_cases <- merge(hosp_cases, comparison_strains, by = 'original_Strain')
colnames(hosp_cases)[1] <- 'pair_Strain'
final_cases <- merge(hosp_cases, final_cases, by = 'pair_Strain')

# We have created a dataframe called 'final_cases' that will use to perform the analysis 

final_cases$pair_year <- str_split_fixed(string = final_cases$Isolation.date.x, pattern = '-', n = 3)[,1]
final_cases$pair_month <- str_split_fixed(string = final_cases$Isolation.date.x, pattern = '-', n = 3)[,2]

final_cases$original_year <- str_split_fixed(string = final_cases$Isolation.date.y, pattern = '-', n = 3)[,1]
final_cases$original_month <- str_split_fixed(string = final_cases$Isolation.date.y, pattern = '-', n = 3)[,2]

final_cases$pair_year_month <- as.numeric(paste(final_cases$pair_year, final_cases$pair_month, sep = ''))
final_cases$original_year_month <- as.numeric(paste(final_cases$original_year, final_cases$original_month, sep = ''))

# We create a new vector (column) so we avoid including twice the same pairwise link (e.g. E7301-E7492, E7492-E7301)

vector_names <- NULL
for(case in 1:nrow(final_cases))
{
  case_row <- final_cases[case,]
  case_names <- c(as.character(case_row$pair_Strain), as.character(case_row$original_Strain))
  case_names <- sort(case_names)
  combi_names <- paste(case_names[1],case_names[2], sep = '-')
  vector_names <- append(x = vector_names, values = combi_names)
}

final_cases$combi_pairwise_name <- vector_names


# Analysis Per year 


whole_information <- NULL

for(yearmonth in sort(unique(final_cases$original_year_month)))
{
    if(yearmonth >= 201501)
    {
      break()
    }
  months_list <- seq(from = yearmonth, to = yearmonth + 100, by = 1)
  window_cases <- subset(final_cases, final_cases$original_year_month %in% months_list)
  window_cases <- subset(window_cases, window_cases$pair_year_month %in% months_list)
  window_cases <- window_cases[! duplicated(window_cases$combi_pairwise_name),]
  eval_plasmid <- window_cases %>%
    count(Evaluation) %>%
    mutate(freq = n / sum(n))
  
  eval_plasmid <- eval_plasmid %>%
    mutate(total_count = sum(n)) 
  
  eval_plasmid <- eval_plasmid %>% 
    mutate(Start_Period = rep(yearmonth),
           Final_Period = rep(yearmonth+100))
  
  whole_information <- rbind(whole_information, eval_plasmid)

}

whole_information$Start_Year <- substr(x = whole_information$Start_Period, start = 1, stop = 4)
whole_information$End_Year <- substr(x = whole_information$Final_Period, start = 1, stop = 4)

whole_information$Period <- paste(whole_information$Start_Year, whole_information$End_Year, sep = '-')

whole_information$Period <- ifelse(whole_information$Period == '2012-2013','Jan. 2012 - Dec. 2013',
ifelse(whole_information$Period == '2013-2014','Jan. 2013 - Dec. 2014',
ifelse(whole_information$Period == '2014-2015','Jan. 2014 - Dec. 2015','Unassigned')))

ggplot(whole_information, aes(x = as.factor(Evaluation), y = freq, fill = as.factor(Evaluation), group = as.factor(Evaluation))) + stat_summary(fun.y="mean", geom="bar") + geom_jitter(size = 1.5, height = 0.0, width = 0.1, alpha = 0.5,color = 'black') + theme_bw() + theme(axis.text.x = element_text(angle = 90), legend.position = 'bottom', text = element_text(size=14))  + labs(x = '', y = 'Relative frequency (Year)', fill = '', title = '') + facet_wrap(Period ~ . , ncol = 4) + scale_fill_brewer(palette = 'Set3')
```

![](report_files/figure-gfm/unnamed-chunk-39-1.png)<!-- -->

``` r
stats_info <- whole_information %>% 
  group_by(Evaluation, Period) %>%
  summarize(mean(freq))
colnames(stats_info)[3] <- 'avg_freq'

stats_info$rounded <- round(stats_info$avg_freq, 2)

final_numbers <- stats_info %>%
  group_by(Evaluation) %>%
  mutate(report = sum(rounded)/sum(stats_info$rounded))

# Analysis per Region 


whole_information <- NULL

for(yearmonth in sort(unique(final_cases$original_year_month)))
{
    if(yearmonth >= 201501)
    {
      break()
    }
  months_list <- seq(from = yearmonth, to = yearmonth + 100, by = 1)
  window_cases <- subset(final_cases, final_cases$original_year_month %in% months_list)
  window_cases <- subset(window_cases, window_cases$pair_year_month %in% months_list)
  
  window_cases <- subset(window_cases, window_cases$Region.x == window_cases$Region.y)
  
  ##Rebuttal
  
  window_cases <- window_cases[! duplicated(window_cases$combi_pairwise_name),]
  
  ## 
  
  eval_plasmid <- window_cases %>%
    group_by(Region.x) %>%
    summarise(Unique_Elements = n_distinct(pair_Strain))
  
  eval_plasmid <- subset(eval_plasmid, eval_plasmid$Unique_Elements >= 10)
  
  window_cases <- subset(window_cases, window_cases$Region.x %in% eval_plasmid$Region.x)
  
  eval_plasmid <- window_cases %>%
    count(Region.x, Evaluation) %>%
    group_by(Region.x) %>%
    mutate(freq = n / sum(n))
  
  eval_plasmid <- eval_plasmid %>%
    group_by(Region.x) %>%
    mutate(total_count = sum(n)) 
  
  eval_plasmid <- eval_plasmid %>% 
    mutate(Start_Period = rep(yearmonth),
           Final_Period = rep(yearmonth+100))
  
  whole_information <- rbind(whole_information, eval_plasmid)

}

whole_information$Start_Year <- substr(x = whole_information$Start_Period, start = 1, stop = 4)
whole_information$End_Year <- substr(x = whole_information$Final_Period, start = 1, stop = 4)

whole_information$Period <- paste(whole_information$Start_Year, whole_information$End_Year, sep = '-')

whole_information$Region_Period <- paste(whole_information$Region.x, whole_information$Period)

whole_information$Region_Period <- gsub(pattern = '2012-2013', replacement = 'Jan. 2012 - Dec. 2013', x = whole_information$Region_Period)
whole_information$Region_Period <- gsub(pattern = '2013-2014', replacement = 'Jan. 2013 - Dec. 2014', x = whole_information$Region_Period)
whole_information$Region_Period <- gsub(pattern = '2014-2015', replacement = 'Jan. 2014 - Dec. 2015', x = whole_information$Region_Period)

ggplot(whole_information, aes(x = as.factor(Evaluation), y = freq, fill = as.factor(Evaluation), group = as.factor(Evaluation))) + stat_summary(fun.y="mean", geom="bar") + geom_jitter(size = 1.5, height = 0.0, width = 0.1, alpha = 0.5,color = 'black') + theme_bw() + theme(axis.text.x = element_text(angle = 90), legend.position = 'bottom', text = element_text(size=14))  + labs(x = '', y = 'Relative frequency (Region/Year)', fill = '', title = '') + facet_wrap(Region_Period ~ . , ncol = 4) + scale_fill_brewer(palette = 'Set3')
```

![](report_files/figure-gfm/unnamed-chunk-39-2.png)<!-- -->

``` r
# Stats behind

stats_info <- whole_information %>% 
  group_by(Evaluation, Region_Period) %>%
  summarize(mean(freq))
colnames(stats_info)[3] <- 'avg_freq'

stats_info$avg_freq <- round(stats_info$avg_freq,4)

stats_info %>%
  group_by(Region_Period) %>%
  summarize(sum(avg_freq))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Region_Period"],"name":[1],"type":["chr"],"align":["left"]},{"label":["sum(avg_freq)"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Flevoland Jan. 2012 - Dec. 2013","2":"1.0000"},{"1":"Limburg Jan. 2012 - Dec. 2013","2":"1.0000"},{"1":"Limburg Jan. 2013 - Dec. 2014","2":"1.0521"},{"1":"Limburg Jan. 2014 - Dec. 2015","2":"0.9999"},{"1":"North-Holland Jan. 2012 - Dec. 2013","2":"1.0000"},{"1":"North-Holland Jan. 2013 - Dec. 2014","2":"1.0000"},{"1":"North-Holland Jan. 2014 - Dec. 2015","2":"1.0000"},{"1":"Overijssel Jan. 2012 - Dec. 2013","2":"1.0317"},{"1":"South-Holland Jan. 2014 - Dec. 2015","2":"1.0000"},{"1":"Utrecht Jan. 2012 - Dec. 2013","2":"1.0000"},{"1":"Utrecht Jan. 2013 - Dec. 2014","2":"1.0028"},{"1":"Utrecht Jan. 2014 - Dec. 2015","2":"1.0000"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Analysis per Hospital 

whole_information <- NULL

for(yearmonth in sort(unique(final_cases$original_year_month)))
{
    if(yearmonth >= 201501)
    {
      break()
    }
  months_list <- seq(from = yearmonth, to = yearmonth + 100, by = 1)
  window_cases <- subset(final_cases, final_cases$original_year_month %in% months_list)
  window_cases <- subset(window_cases, window_cases$pair_year_month %in% months_list)
  
  window_cases <- subset(window_cases, window_cases$Hospital.x == window_cases$Hospital.y)
  
  window_cases <- window_cases[! duplicated(window_cases$combi_pairwise_name),]
  
  eval_plasmid <- window_cases %>%
    group_by(Hospital.x) %>%
    summarise(Unique_Elements = n_distinct(pair_Strain))
  
  eval_plasmid <- subset(eval_plasmid, eval_plasmid$Unique_Elements >= 10)
  
  window_cases <- subset(window_cases, window_cases$Hospital.x %in% eval_plasmid$Hospital.x)
  
  eval_plasmid <- window_cases %>%
    count(Hospital.x, Evaluation) %>%
    group_by(Hospital.x) %>%
    mutate(freq = n / sum(n))
  
  eval_plasmid <- eval_plasmid %>%
    group_by(Hospital.x) %>%
    mutate(total_count = sum(n)) 
  
  eval_plasmid <- eval_plasmid %>% 
    mutate(Start_Period = rep(yearmonth),
           Final_Period = rep(yearmonth+100))
  
  whole_information <- rbind(whole_information, eval_plasmid)

}

whole_information$Start_Year <- substr(x = whole_information$Start_Period, start = 1, stop = 4)
whole_information$End_Year <- substr(x = whole_information$Final_Period, start = 1, stop = 4)

whole_information$Period <- paste(whole_information$Start_Year, whole_information$End_Year, sep = '-')

whole_information$Hosp_Period <- paste(whole_information$Hospital.x, whole_information$Period)
whole_information$Hosp_Period <- gsub(pattern = '2012-2013', replacement = 'Jan. 2012 - Dec. 2013', x = whole_information$Hosp_Period)
whole_information$Hosp_Period <- gsub(pattern = '2013-2014', replacement = 'Jan. 2013 - Dec. 2014', x = whole_information$Hosp_Period)
whole_information$Hosp_Period <- gsub(pattern = '2014-2015', replacement = 'Jan. 2014 - Dec. 2015', x = whole_information$Hosp_Period)

ggplot(whole_information, aes(x = as.factor(Evaluation), y = freq, fill = as.factor(Evaluation), group = as.factor(Evaluation))) + stat_summary(fun.y="mean", geom="bar") + geom_jitter(size = 1.5, height = 0.0, width = 0.1, alpha = 0.5,color = 'black') + theme_bw() + theme(axis.text.x = element_text(angle = 90), legend.position = 'bottom', text = element_text(size=14))  + labs(x = '', y = 'Relative frequency (Hospital/Year)', fill = '', title = '') + facet_wrap(Hosp_Period ~ . , ncol = 3, nrow = 5) + scale_fill_brewer(palette = 'Set3')
```

![](report_files/figure-gfm/unnamed-chunk-39-3.png)<!-- -->

# R session

These are the libraries and dependencies loaded to run the analysis

``` r
sessionInfo()
```

    ## R version 3.5.1 RC (2018-06-24 r74934)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 14.04.6 LTS
    ## 
    ## Matrix products: default
    ## BLAS: /usr/lib/libblas/libblas.so.3.0
    ## LAPACK: /usr/lib/lapack/liblapack.so.3.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=nl_NL.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=nl_NL.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=nl_NL.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=nl_NL.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] sp_1.4-2           gplots_3.0.1.1     RColorBrewer_1.1-2
    ##  [4] scatterpie_0.1.5   maps_3.3.0         rcartocolor_2.1.0 
    ##  [7] igraph_1.2.4.1     ape_5.3            ggtree_1.14.6     
    ## [10] cowplot_0.9.4      ggridges_0.5.2     forcats_0.4.0     
    ## [13] stringr_1.4.0      dplyr_0.8.0.1      purrr_0.3.2       
    ## [16] readr_1.3.1        tidyr_0.8.3        tibble_2.1.1      
    ## [19] ggplot2_3.1.0      tidyverse_1.2.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Rcpp_1.0.1         lubridate_1.7.4    lattice_0.20-38   
    ##  [4] gtools_3.8.1       assertthat_0.2.1   digest_0.6.18     
    ##  [7] ggforce_0.2.2      R6_2.4.0           cellranger_1.1.0  
    ## [10] plyr_1.8.4         backports_1.1.3    evaluate_0.13     
    ## [13] httr_1.4.0         pillar_1.3.1       rlang_0.4.2       
    ## [16] lazyeval_0.2.2     readxl_1.3.1       gdata_2.18.0      
    ## [19] rstudioapi_0.10    rmarkdown_1.12     labeling_0.3      
    ## [22] polyclip_1.10-0    munsell_0.5.0      broom_0.5.1       
    ## [25] compiler_3.5.1     modelr_0.1.4       xfun_0.6          
    ## [28] pkgconfig_2.0.2    htmltools_0.3.6    tidyselect_0.2.5  
    ## [31] crayon_1.3.4       withr_2.1.2        bitops_1.0-6      
    ## [34] MASS_7.3-51.3      grid_3.5.1         nlme_3.1-137      
    ## [37] jsonlite_1.6       gtable_0.3.0       magrittr_1.5      
    ## [40] scales_1.0.0       KernSmooth_2.23-15 tidytree_0.2.4    
    ## [43] cli_1.1.0          stringi_1.4.3      mapproj_1.2.7     
    ## [46] farver_1.1.0       xml2_1.2.0         rvcheck_0.1.3     
    ## [49] generics_0.0.2     tools_3.5.1        treeio_1.6.2      
    ## [52] glue_1.3.1         tweenr_1.0.1       hms_0.4.2         
    ## [55] parallel_3.5.1     yaml_2.2.0         colorspace_1.4-1  
    ## [58] caTools_1.17.1.2   rvest_0.3.2        knitr_1.22        
    ## [61] haven_2.1.0
